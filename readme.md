Flee V4 iPad App (iOS 7) by: Andrew McCallum

When opening project please open using this file: FleeV4.xcworkspace

Flee’s service allows users to view and book a series of hotel and travel recommendations to some of North America's most prized yet overlooked destinations based on several search parameters. The iPad app will allow users to view Flee in a custom native experience that involves creating custom favorite location lists, sharing locations via Facebook/Twitter, and a login system. The app communicates with a custom back-end API using the AFNetworking framework and will be released in iOS 7. 


Features:

-custom login/signup system with form validation

-custom collection views with content dynamically loaded from custom api

-flight and hotel booking attached via webviews

Issues:

-the search back-end is under construction, and is not implemented in the app yet

-the content and pictures are still a work in progress, and venue pictures will all appear to be the same until new pictures are added to the database

-some of the design in the app is not complete, but all of the functionality is there