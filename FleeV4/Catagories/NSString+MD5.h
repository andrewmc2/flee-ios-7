//
//  NSString+MD5.h
//  FleeLoginTest
//
//  Created by Andrew McCallum14 on 2013-07-22.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
