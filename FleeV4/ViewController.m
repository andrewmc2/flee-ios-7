//
//  ViewController.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-21.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "ViewController.h"
#import "HeaderView.h"
#import "MainPageSearchAndCollectionViewScrollView.h"
#import <AFNetworking.h>
#import "LoginRegisterViewController.h"
#import <UICKeyChainStore.h>
#import "API.h"
#import "MainPageCollectionViewCustomCell.h"
#import "FleeSloganCollectionReusableFooterView.h"
#import "FavoriteJSONParser.h"
#import "SearchJSONParser.h"
#import "CityViewController.h"
#import "CityAllJSONParser.h"
#import "FirstLaunchSingleton.h"
#import "FirstLaunchMainViewControlerModal.h"

@interface ViewController ()

@property (nonatomic) BOOL mainPageHasBeenViewed;
@property (nonatomic) BOOL menuIsDroppedDown;
@property (strong, nonatomic) NSMutableDictionary *user;
@property (strong, nonatomic) MainPageSearchAndCollectionViewScrollView *mainPageSearchAndCollectionViewScrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic) int selectedCityCell;
@property (strong, nonatomic) NSMutableArray *cityArrayForAllCities;
@property (strong, nonatomic) NSString *venueIdForVenueSegue;

@end

@implementation ViewController

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIViewController*)sender{
    CityViewController *cityViewController = [[CityViewController alloc] init];
    cityViewController = [segue destinationViewController];
    cityViewController.selectedCityCell = self.selectedCityCell;
    [cityViewController setSegueToVenueAutomatically:NO];
    cityViewController.delegate = self;
    
    if ([segue.identifier isEqualToString:@"City"] && sender.view.tag == 1) {
        [cityViewController setSegueToVenueAutomatically:YES];
        [cityViewController setVenueIdForVenueSegue:self.venueIdForVenueSegue];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setupMainScrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    if (!self.mainPageHasBeenViewed) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
        {
            // app already launched
            NSLog(@"app has launched before");
            
            //uncomment if you want tutorial on every launch
//            [[FirstLaunchSingleton sharedSingleton] setLoginRegisterViewControllerFirstLaunch:YES];
        } else
        {
            [[FirstLaunchSingleton sharedSingleton] setLoginRegisterViewControllerFirstLaunch:YES];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            // This is the first launch ever
            [UICKeyChainStore setString:@"" forKey:@"username" service:@"flee"];
            NSLog(@"first launch ever");
        }
        
        NSString *user = [UICKeyChainStore stringForKey:@"username" service:@"flee"];
        NSString *password = [UICKeyChainStore stringForKey:@"password" service:@"flee"];
        
        if ([user isEqualToString:@""]) {
            LoginRegisterViewController *loginRegisterViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegister"];
            [self presentViewController:loginRegisterViewController animated:NO completion:^{
                if ([[FirstLaunchSingleton sharedSingleton] loginRegisterViewControllerFirstLaunch]) {
                    [loginRegisterViewController presentIntroView];
                }
            }];
            
        } else {
            NSMutableDictionary *loginParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                user, @"username",
                                                password, @"password",
                                                nil];
            [[API sharedInstance] loginWithParams:loginParams onCompletion:^(NSDictionary *json) {
                [self.spinner startAnimating];
                
                if ([json objectForKey:@"error"]) {
                    UIAlertView *noInternetAlertView = [[UIAlertView alloc] initWithTitle:@"Warning" message:@"No Internet" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [noInternetAlertView show];
                    [self.spinner stopAnimating];
                } else {
                    //setup favorites singleton
                    [[FavoriteJSONParser sharedSingleton] setUser:[json mutableCopy]];
                    [FavoriteJSONParser sharedSingleton].delegate = self;
                    [[FavoriteJSONParser sharedSingleton] createFavoritedCityandVenueArrays];
                    [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView reloadData];
                    
                    //setup initial cities in main collection view
                    [SearchJSONParser sharedSingleton];
                    AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[SearchJSONParser sharedSingleton].request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                        [SearchJSONParser sharedSingleton].citySearchJSON = [NSMutableArray arrayWithArray:(NSMutableArray*)JSON];
                        [self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView reloadData];
                        [self.spinner stopAnimating];
                        self.mainPageHasBeenViewed = YES;
                    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
                    }];
                    
                    [requestOperation start];
                    if ([[FirstLaunchSingleton sharedSingleton] loginRegisterViewControllerFirstLaunch]) {
                        [self addIntroScreen];
                    }
                    }
                }
             ];
            
            //setup a json object for all cities
            [CityAllJSONParser sharedSingleton];
            AFJSONRequestOperation *requestOperation = [AFJSONRequestOperation JSONRequestOperationWithRequest:[CityAllJSONParser sharedSingleton].request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
                [SearchJSONParser sharedSingleton].cityAllJSON = [NSMutableArray arrayWithArray:(NSMutableArray*)JSON];
                self.cityArrayForAllCities = [[SearchJSONParser sharedSingleton] createCityAllArray];
            } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {
            }];
            
            [requestOperation start];
        }
    } else {
        [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView reloadData];
        [self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView reloadData];
    }
}

#pragma mark setup vc
-(void)setupMainScrollView{
    //add main header view
    HeaderView *headerView = [[HeaderView alloc] init];
    [self.mainScrollView addSubview:headerView];
    [headerView.logOutButton addTarget:self
                                action:@selector(logOut:)
                      forControlEvents:UIControlEventTouchUpInside];
    [headerView.ribbonPullDown addTarget:self
                                  action:@selector(buttonTapped:)
                        forControlEvents:UIControlEventTouchDown];
    self.mainPageSearchAndCollectionViewScrollView = [[MainPageSearchAndCollectionViewScrollView alloc] init];
    [self.mainScrollView addSubview:self.mainPageSearchAndCollectionViewScrollView];
    self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView.delegate = self;
    self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView.dataSource = self;
    self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView.delegate = self;
    self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView.dataSource = self;
}

#pragma mark @selector functions
-(IBAction)buttonTapped:(id)sender{
    if (!self.menuIsDroppedDown) {
        [self.mainScrollView setContentOffset:CGPointMake(0, -260) animated:YES];
        self.menuIsDroppedDown = YES;
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(0, -20) animated:YES];
        self.menuIsDroppedDown = NO;
    }
}

-(IBAction)logOut:(id)sender{
    [UICKeyChainStore setString:@"" forKey:@"username" service:@"flee"];
    [UICKeyChainStore setString:@"" forKey:@"password" service:@"flee"];
    LoginRegisterViewController *loginRegisterViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegister"];
    [self buttonTapped:sender];
    
    [self presentViewController:loginRegisterViewController animated:YES completion:nil];
}

-(IBAction)favoriteCollectionViewHeartPressed:(UIButton*)sender{
    //start spinner
    [self.spinner startAnimating];
    
    //create array out of titleLabel string
    NSArray *titleLabelInfoArray = [sender.titleLabel.text componentsSeparatedByString:@","];
    
    //pop array
    [[FavoriteJSONParser sharedSingleton] popArray:[[titleLabelInfoArray objectAtIndex:2] integerValue] typeOfArray:[titleLabelInfoArray objectAtIndex:3]];
    //delete cell
        NSIndexPath *indexPathForCityFavoriteDeletion = [NSIndexPath indexPathForRow:[[titleLabelInfoArray objectAtIndex:2] integerValue] inSection:[[titleLabelInfoArray objectAtIndex:1] integerValue]];
        NSArray *indexPathsForCityDeletionArray = [NSArray arrayWithObject:indexPathForCityFavoriteDeletion];
        [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView performBatchUpdates:^{
            NSMutableArray *favCityArrayFromUserJSON = [NSMutableArray arrayWithArray:[[FavoriteJSONParser sharedSingleton].user objectForKey:[titleLabelInfoArray objectAtIndex:3]]];
            [favCityArrayFromUserJSON removeObjectAtIndex:[[titleLabelInfoArray objectAtIndex:2] integerValue]];
            [[FavoriteJSONParser sharedSingleton].user setObject:favCityArrayFromUserJSON forKey:[titleLabelInfoArray objectAtIndex:3]];
            [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView deleteItemsAtIndexPaths:indexPathsForCityDeletionArray];
        } completion:^(BOOL finished) {
            [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView reloadSections:[NSIndexSet indexSetWithIndex:[[titleLabelInfoArray objectAtIndex:1] integerValue]]];
            [self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView reloadData];
            //delete on server
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[[FavoriteJSONParser sharedSingleton].user objectForKey:@"username"], @"username", [titleLabelInfoArray objectAtIndex:0], @"cityname", [titleLabelInfoArray objectAtIndex:4], @"venue_id", nil];
            
            [[API sharedInstance] deleteFavorite:params onCompletion:^(NSDictionary *json) {
                NSLog(@"deleted");
            }];
        }];
    //stop spinner
    [self.spinner stopAnimating];
}

#pragma mark collection view delegate/datasource methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
if (collectionView == self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView) {
        return 1;
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView) {
        return 2;
    }
    
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView) {
        return [[SearchJSONParser sharedSingleton].citySearchJSON count];
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView && section == 0) {
        return [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_cities"] count];
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView && section == 1) {
        return [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"] count];
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    MainPageCollectionViewCustomCell *cell = (MainPageCollectionViewCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"itemIdentifier" forIndexPath:indexPath];
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView) {
        [cell.imageView setImageWithURL:[NSURL URLWithString:[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"PIC_URL"]]];
        cell.cityName.text = [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"NAME"];
        cell.cityURL = [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"URL"];
        cell.favoritedButton.selected = [[FavoriteJSONParser sharedSingleton].favoritedCityArray containsObject:cell.cityURL];
        cell.favoritedButton.titleLabel.text = [NSString stringWithFormat:@"%@,%i,%i,Fav_cities,%@",cell.cityURL,indexPath.section,indexPath.row,@""];
        [cell.favoritedButton addTarget:[FavoriteJSONParser sharedSingleton] action:@selector(favoritesHeartPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView && indexPath.section == 0) {
        [cell.imageView setImageWithURL:[NSURL URLWithString:[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_cities"][indexPath.row] objectForKey:@"PIC_URL"]]];
        cell.cityName.text = [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_cities"][indexPath.row] objectForKey:@"NAME"];
        cell.cityURL = [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_cities"][indexPath.row] objectForKey:@"URL"];
        cell.favoritedButton.selected = [[FavoriteJSONParser sharedSingleton].favoritedCityArray containsObject:cell.cityURL];
        cell.favoritedButton.titleLabel.text = [NSString stringWithFormat:@"%@,%i,%i,Fav_cities,%@",cell.cityURL,indexPath.section,indexPath.row,@""];
        [cell.favoritedButton addTarget:self action:@selector(favoriteCollectionViewHeartPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView && indexPath.section == 1) {
        [cell.imageView setImageWithURL:[NSURL URLWithString:[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"][indexPath.row] objectForKey:@"pic_url"]]];
        cell.cityName.text = [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"][indexPath.row] objectForKey:@"name"];
        cell.cityURL = [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"][indexPath.row] objectForKey:@"CityName"];
        cell.venueCode = [[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"][indexPath.row] objectForKey:@"VenueCode"];
        cell.favoritedButton.selected = [[FavoriteJSONParser sharedSingleton].favoritedVenueArray containsObject:[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"][indexPath.row] objectForKey:@"id"]];
        cell.favoritedButton.titleLabel.text = [NSString stringWithFormat:@"%@,%i,%i,Fav_venues,%@",cell.cityURL,indexPath.section,indexPath.row, cell.venueCode];
        [cell.favoritedButton addTarget:self action:@selector(favoriteCollectionViewHeartPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView) {
        self.view.tag = 0;
        self.selectedCityCell = indexPath.row;
        [self performSegueWithIdentifier:@"City" sender:self];
    }
    
    if (collectionView == self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView) {

        if (indexPath.section == 0) {
            self.view.tag = 0;
            NSString *cityUrl = [[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_cities"] objectAtIndex:indexPath.row] objectForKey:@"URL"];
            int indexRowForCityUrl = [self.cityArrayForAllCities indexOfObject:cityUrl];
            self.selectedCityCell = indexRowForCityUrl;
            [SearchJSONParser sharedSingleton].citySearchJSON = [SearchJSONParser sharedSingleton].cityAllJSON;
            [self performSegueWithIdentifier:@"City" sender:self];
        }
        
        if (indexPath.section == 1) {
            self.view.tag = 1;
            NSString *cityUrlForVenueSegue = [[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"] objectAtIndex:indexPath.row] objectForKey:@"city"];
            int indexRowForCityUrl = [self.cityArrayForAllCities indexOfObject:cityUrlForVenueSegue];
            self.selectedCityCell = indexRowForCityUrl;
            [SearchJSONParser sharedSingleton].citySearchJSON = [SearchJSONParser sharedSingleton].cityAllJSON;
            self.venueIdForVenueSegue = [[[[FavoriteJSONParser sharedSingleton].user objectForKey:@"Fav_venues"] objectAtIndex:indexPath.row] objectForKey:@"id"];
            [self performSegueWithIdentifier:@"City" sender:self];
        }

    }
}

#pragma mark temp things

-(void)reloadCollectionViews{
    [self.mainPageSearchAndCollectionViewScrollView setContentOffset:CGPointZero animated:YES];
    [self.mainPageSearchAndCollectionViewScrollView.favoritesCollectionView reloadData];
    [self.mainPageSearchAndCollectionViewScrollView.mainPageCollectionView reloadData];
}

-(void)addIntroScreen{
    FirstLaunchMainViewControlerModal *firstLaunchMainViewControlerModal = [[FirstLaunchMainViewControlerModal alloc] init];
    [self.view addSubview:firstLaunchMainViewControlerModal];
    [UIView animateWithDuration:0.5f animations:^{
        [firstLaunchMainViewControlerModal setFrame:CGRectMake(0, 0, 768, 1024)];
    } completion:^(BOOL finished) {
    }];
}

@end