//
//  CityViewController.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RefreshFavoriteCollectionViewDelegate.h"

@interface CityViewController : UIViewController <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) UIButton *ribbonPullDown;
@property (strong, nonatomic) UICollectionView *cityCollectionView;
@property (nonatomic) int selectedCityCell;

@property (nonatomic) BOOL segueToVenueAutomatically;
@property (strong, nonatomic) NSString *venueIdForVenueSegue;;
@property (strong, nonatomic) id <ReloadCollectionViewsDelegate> delegate;

@end
