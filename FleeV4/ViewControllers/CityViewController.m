//
//  CityViewController.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "CityViewController.h"
#import "CityViewControllerCollectionViewFlowLayout.h"
#import "CityandVenueHeaderView.h"
#import "LoginRegisterViewController.h"
#import <UICKeyChainStore.h>
#import "CityViewControllerCollectionViewCustomCell.h"
#import "SearchJSONParser.h"
#import "UIImageView+AFNetworking.h"
#import "FavoriteJSONParser.h"
#import "VenueImageViewForCityCollectionViewCell.h"
#import "VenueViewController.h"
#import "FlightBookingModalView.h"
#import "BookFlightViewController.h"
#import "FirstLaunchSingleton.h"
#import "FirstLaunchCityViewControllerModal.h"

@interface CityViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic) BOOL menuIsDroppedDown;
@property (strong, nonatomic) SLComposeViewController *slComposeViewController;
@property (strong, nonatomic) UIActivityViewController *uiActivityViewController;
@property (strong, nonatomic) NSString *cityShareSentence;
@property (strong, nonatomic) NSURL *cityShareLink;
@property (nonatomic) int selectedVenueRow;
@property (nonatomic) int venueCount;
@end

@implementation CityViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"Venue"]) {
        VenueViewController *venueViewController = [[VenueViewController alloc] init];
        venueViewController = [segue destinationViewController];
        venueViewController.selectedVenueCell = self.selectedVenueRow;
        venueViewController.selectedCityCell = self.selectedCityCell;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupMainScrollView];
    [self goToSelectedCity];
    
    if ([[FirstLaunchSingleton sharedSingleton] loginRegisterViewControllerFirstLaunch]) {
        [self addIntroScreen];
        [[FirstLaunchSingleton sharedSingleton] setLoginRegisterViewControllerFirstLaunch:NO];
    }
    if (self.segueToVenueAutomatically) {
        self.segueToVenueAutomatically = NO;
        for (NSDictionary *venueDict in [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"]) {
            if ([[venueDict objectForKey:@"id"] isEqualToString:self.venueIdForVenueSegue]) {
                int venueIndex = [[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] indexOfObject:venueDict];
                self.selectedVenueRow = venueIndex;
            }
        }
        [self performSelector:@selector(segueToNextViaFavorite:) withObject:nil afterDelay:1.0];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    //if i want collection view to scroll to top upon pressing back
    [self.cityCollectionView reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupMainScrollView{
    //add collectionview
    CityViewControllerCollectionViewFlowLayout *cityViewControllerCollectionViewFlowLayout = [[CityViewControllerCollectionViewFlowLayout alloc] init];
    self.cityCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 768, 1284) collectionViewLayout:cityViewControllerCollectionViewFlowLayout];
    [self.cityCollectionView registerClass:[CityViewControllerCollectionViewCustomCell class] forCellWithReuseIdentifier:@"cityCustomCell"];
    self.cityCollectionView.dataSource = self;
    self.cityCollectionView.delegate = self;
    self.cityCollectionView.pagingEnabled = YES;
    [self.mainScrollView addSubview:self.cityCollectionView];
    
    //add main header view
    CityandVenueHeaderView *cityandVenueHeaderView = [[CityandVenueHeaderView alloc] init];
    [self.mainScrollView addSubview:cityandVenueHeaderView];
    [cityandVenueHeaderView.logOutButton addTarget:self
                                action:@selector(logOut:)
                      forControlEvents:UIControlEventTouchUpInside];
    
    //ribbon pull down
    self.ribbonPullDown = [UIButton buttonWithType:UIButtonTypeCustom];
    self.ribbonPullDown.frame = CGRectMake(666.5, 260, 35.0, 88.0);
    [self.ribbonPullDown setImage:[UIImage imageNamed:@"ribbonPullDown.png"] forState:UIControlStateNormal];
    [self.mainScrollView addSubview:self.ribbonPullDown];
    [self.ribbonPullDown addTarget:self
                                  action:@selector(buttonTapped:)
                        forControlEvents:UIControlEventTouchDown];
}

-(void)goToSelectedCity{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedCityCell inSection:0];
    [self.cityCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

#pragma mark @selector functions
-(IBAction)buttonTapped:(id)sender{
    if (!self.menuIsDroppedDown) {
        [self.mainScrollView setContentOffset:CGPointMake(0, -260) animated:YES];
        self.menuIsDroppedDown = YES;
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(0, -20) animated:YES];
        self.menuIsDroppedDown = NO;
    }
}

-(IBAction)goBackToHomePage:(UIButton*)sender{
    [self.delegate reloadCollectionViews];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)logOut:(id)sender{
    [UICKeyChainStore setString:@"" forKey:@"username" service:@"flee"];
    [UICKeyChainStore setString:@"" forKey:@"password" service:@"flee"];
    LoginRegisterViewController *loginRegisterViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegister"];
    [self buttonTapped:sender];
    [self presentViewController:loginRegisterViewController animated:YES completion:nil];
}

-(IBAction)shareButtonPressed:(id)sender{
    
    NSArray *dataToShare = @[self.cityShareSentence,self.cityShareLink];
    
    self.uiActivityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    self.uiActivityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeMessage, UIActivityTypeCopyToPasteboard,UIActivityTypePostToWeibo, UIActivityTypeAssignToContact];
    [self presentViewController:self.uiActivityViewController animated:YES completion:^{
        //stuff
    }];
}

-(IBAction)bookFlightButtonPressed:(UIButton*)sender{
    BookFlightViewController *bookFlightViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"flight"];
    [bookFlightViewController setArrivalCityAirport:[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"CODE"]];
    [bookFlightViewController setCityNameBookingSentenceString:[NSString stringWithFormat:@"Choose your departure city, departure date, and number of nights for your trip to %@",[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"NAME"]]];
    [self presentViewController:bookFlightViewController animated:YES completion:nil];
}

-(IBAction)segueToVenueViewController:(UITapGestureRecognizer*)sender{
    self.selectedVenueRow = sender.view.tag;
    [self performSegueWithIdentifier:@"Venue" sender:self];
}

-(IBAction)segueToNextViaFavorite:(id)sender{
    [self performSegueWithIdentifier:@"Venue" sender:self];
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[SearchJSONParser sharedSingleton].citySearchJSON count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    CityViewControllerCollectionViewCustomCell *cell = (CityViewControllerCollectionViewCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cityCustomCell" forIndexPath:indexPath];
    //go to top of cell
    [cell.mainScrollView setContentOffset:CGPointZero animated:NO];
    
    //setscrollviewsize
    [cell setContentSize:[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"venues"] count]];
    
    //main image
    [cell.cityImageView setImageWithURL:[NSURL URLWithString:[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row]objectForKey:@"PIC_URL"]]];
    //back button
    [cell.backButton addTarget:self action:@selector(goBackToHomePage:) forControlEvents:UIControlEventTouchDown];
    //city name
    cell.cityName.text = (NSString*)[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"NAME"] capitalizedString];
    //cityURL
    cell.cityURL = [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"URL"];
    //favorite button
    cell.favoritedButton.selected = [[FavoriteJSONParser sharedSingleton].favoritedCityArray containsObject:cell.cityURL];
    cell.favoritedButton.titleLabel.text = [NSString stringWithFormat:@"%@,%i,%i,Fav_cities,%@",cell.cityURL,indexPath.section,indexPath.row,@""];
    [cell.favoritedButton addTarget:[FavoriteJSONParser sharedSingleton] action:@selector(favoritesHeartPressed:) forControlEvents:UIControlEventTouchUpInside];
    //infoscrollview to zero
    [cell.infoScrollView setContentOffset:CGPointZero animated:NO];
    //city description
    cell.cityDescriptionTextView.text = [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"SHORT"];
    //share button
    [cell.shareToSocialMediaButton addTarget:self action:@selector(shareButtonPressed:) forControlEvents:UIControlEventTouchDown];
    self.cityShareSentence = [NSString stringWithFormat:@"Check out the #setmeflee %@ city guide", (NSString*)cell.cityName.text];
    self.cityShareLink = [NSURL URLWithString:[NSString stringWithFormat:@"http://setmeflee.com/destinations/%@",(NSString*)cell.cityURL]];
    //book flight button
    [cell.bookFlightButton addTarget:self action:@selector(bookFlightButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    //venues
    int firstVenueYPos = 1023;
    int venueTag = 0;
    
    for (NSDictionary *venue in [[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"venues"]) {
        [cell addVenueToCollectionViewCell:firstVenueYPos];
        cell.venueImageView.venueLabel.text = [venue objectForKey:@"name"];
        [cell.venueImageView setImageWithURL:[NSURL URLWithString:@"http://www.setmeflee.com/assets/venue_imgs/thematador.jpg"] placeholderImage:[UIImage imageNamed:@"fleeCollectionViewCellPlaceholderImage"]];
        cell.venueImageView.tag = venueTag;
        venueTag++;
        firstVenueYPos += cell.venueImageView.frame.size.height;
        [cell.venueImageView.tapGestureRecognizer addTarget:self action:@selector(segueToVenueViewController:)];
    }
    
    for (int i = 0; i < 2; i++) {
        [cell addVenueToCollectionViewCell:firstVenueYPos];
        [cell.venueImageView setImage:[UIImage imageNamed:@"fleeCollectionViewCellPlaceholderImage"]];
        firstVenueYPos += cell.venueImageView.frame.size.height;
    }
    
    //count venues
    self.venueCount = [[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:indexPath.row] objectForKey:@"venues"] count];
    self.selectedCityCell = indexPath.row;
    return cell;
}

-(void)addIntroScreen{
    FirstLaunchCityViewControllerModal *firstLaunchCityViewControllerModal = [[FirstLaunchCityViewControllerModal alloc] init];
    [self.view addSubview:firstLaunchCityViewControllerModal];
    [UIView animateWithDuration:0.5f animations:^{
        [firstLaunchCityViewControllerModal setFrame:CGRectMake(0, 0, 768, 1024)];
    } completion:^(BOOL finished) {
    }];
}
@end
