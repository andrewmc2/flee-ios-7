//
//  VenueViewController.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-26.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "VenueViewController.h"
#import "CityandVenueHeaderView.h"
#import <UICKeyChainStore.h>
#import "LoginRegisterViewController.h"
#import "SearchJSONParser.h"
#import "UIImageView+AFNetworking.h"
#import "FavoriteJSONParser.h"
#import "VenueViewControllerCollectionViewCustomCell.h"
#import "VenueViewControllerCollectionViewFlowLayout.h"
#import "VenueForMap.h"
#import "BookHotelModalView.h"
#import "HotelBookingObject.h"
#import "CityViewController.h"
#import "BookHotelViewController.h"
#import "ViewWebSiteView.h"

@interface VenueViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic) BOOL menuIsDroppedDown;
@property (strong, nonatomic) UIActivityViewController *uiActivityViewController;
@property (strong, nonatomic) NSString *venueShareSentence;
@property (strong, nonatomic) NSURL *venueShareLink;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableDictionary *venueDict;
@property (weak, nonatomic) IBOutlet UILabel *fleeSlogan;
@property (strong, nonatomic) HotelBookingObject *hotelBookingObject;
@property (nonatomic) int currentVenueCell;
@property (strong, nonatomic) NSString *currentVenueCellId;
@property (strong, nonatomic) NSString *currentVenueName;

@end

@implementation VenueViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupMainScrollView];
    [self goToSelectedCity];
}

-(void)goToSelectedCity{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedVenueCell inSection:0];
    [self.venueCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupMainScrollView{
    //add collectionview
    VenueViewControllerCollectionViewFlowLayout *venueViewControllerCollectionViewFlowLayout = [[VenueViewControllerCollectionViewFlowLayout alloc] init];
    self.venueCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 260, 768, 640) collectionViewLayout:venueViewControllerCollectionViewFlowLayout];
    [self.venueCollectionView registerClass:[VenueViewControllerCollectionViewCustomCell class] forCellWithReuseIdentifier:@"venueCustomCell"];
    self.venueCollectionView.dataSource = self;
    self.venueCollectionView.delegate = self;
    self.venueCollectionView.pagingEnabled = YES;
    [self.mainScrollView addSubview:self.venueCollectionView];
    
    
    //add main header view
    CityandVenueHeaderView *cityandVenueHeaderView = [[CityandVenueHeaderView alloc] init];
    [self.mainScrollView addSubview:cityandVenueHeaderView];
    [cityandVenueHeaderView.logOutButton addTarget:self
                                            action:@selector(logOut:)
                                  forControlEvents:UIControlEventTouchUpInside];
    [cityandVenueHeaderView.logoButton addTarget:self action:@selector(goBackToHomePage:) forControlEvents:UIControlEventTouchDown];
    
    //ribbon pull down
    self.ribbonPullDown = [UIButton buttonWithType:UIButtonTypeCustom];
    self.ribbonPullDown.frame = CGRectMake(666.5, 260, 35.0, 88.0);
    [self.ribbonPullDown setImage:[UIImage imageNamed:@"ribbonPullDown.png"] forState:UIControlStateNormal];
    [self.mainScrollView addSubview:self.ribbonPullDown];
    [self.ribbonPullDown addTarget:self
                            action:@selector(buttonTapped:)
                  forControlEvents:UIControlEventTouchDown];
    
    //flee slogan
    [self.fleeSlogan setTextColor:[UIColor colorWithRed:(210/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0f]];
    [self.fleeSlogan setFont:[UIFont fontWithName:@"EgyptienneFLTStd-Roman" size:15.0f]];
    
    //hotel availability object
    self.hotelBookingObject = [[HotelBookingObject alloc] init];
    
}

#pragma mark @selector functions
-(IBAction)buttonTapped:(id)sender{
    if (!self.menuIsDroppedDown) {
        [self.mainScrollView setContentOffset:CGPointMake(0, -260) animated:YES];
        self.menuIsDroppedDown = YES;
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(0, -20) animated:YES];
        self.menuIsDroppedDown = NO;
    }
}

-(IBAction)goBackToHomePage:(UIButton*)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)logOut:(id)sender{
    [UICKeyChainStore setString:@"" forKey:@"username" service:@"flee"];
    [UICKeyChainStore setString:@"" forKey:@"password" service:@"flee"];
    LoginRegisterViewController *loginRegisterViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegister"];
    [self buttonTapped:sender];
    
    [self presentViewController:loginRegisterViewController animated:YES completion:nil];
}

-(IBAction)shareButtonPressed:(id)sender{
    
    NSArray *dataToShare = @[self.venueShareSentence,self.venueShareLink];
    
    self.uiActivityViewController = [[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    self.uiActivityViewController.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeMessage, UIActivityTypeCopyToPasteboard,UIActivityTypePostToWeibo, UIActivityTypeAssignToContact];
    [self presentViewController:self.uiActivityViewController animated:YES completion:^{
        //stuff
    }];
}

-(IBAction)bookHotelButtonPressed:(UIButton*)sender{
    
        if ([self.hotelBookingObject.mrSmithDict objectForKey:self.currentVenueCellId]!=nil) {
            BookHotelViewController *bookHotelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"hotel"];
            [bookHotelViewController setVenueID:self.currentVenueCellId];
            [bookHotelViewController setTypeOfWebView:@"mrsmith"];
            
            [self presentViewController:bookHotelViewController animated:YES completion:^{
                [bookHotelViewController setHotelNameBookingSentenceString:[NSString stringWithFormat:@"Choose your check-in date and number of nights for your stay to %@",self.currentVenueName]];
            }];
        } else if ([self.hotelBookingObject.travelocityDict objectForKey:self.currentVenueCellId]!=nil) {
            BookHotelViewController *bookHotelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"hotel"];
            [bookHotelViewController setVenueID:self.currentVenueCellId];
            [bookHotelViewController setTypeOfWebView:@"travelocity"];
            [bookHotelViewController setHotelNameBookingSentenceString:[NSString stringWithFormat:@"Choose your check-in date and number of nights for your stay to %@",self.currentVenueName]];
            [self presentViewController:bookHotelViewController animated:YES completion:nil];
        } else {
            ViewWebSiteView *viewWebSiteView = [[ViewWebSiteView alloc] init];
            [self.view addSubview:viewWebSiteView];
            [viewWebSiteView setUrlString:[[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:self.currentVenueCell] objectForKey:@"link"]];
            [viewWebSiteView setupWebView];
            
            [UIView animateWithDuration:0.5f animations:^{
                [viewWebSiteView setFrame:CGRectMake(0, 0, 768, 1024)];
            } completion:^(BOOL finished) {
            }];
        }
}

#pragma mark UICollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedVenueCell] objectForKey:@"venues"] count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    VenueViewControllerCollectionViewCustomCell *cell = (VenueViewControllerCollectionViewCustomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"venueCustomCell" forIndexPath:indexPath];
    
    self.currentVenueCell = indexPath.row;
    
    //image
    NSURL *url = [NSURL URLWithString:@"http://www.setmeflee.com/assets/venue_imgs/thematador.jpg"];
    [cell.venueImageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"fleeCollectionViewCellPlaceholderImage"]];
    //back button
    [cell.backButton addTarget:self action:@selector(goBackToHomePage:) forControlEvents:UIControlEventTouchDown];
    //title
    cell.venueName.text = [[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    //urls
    cell.cityURL =[[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"city"];
    cell.venueURL = [[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"id"];
    self.currentVenueCellId = cell.venueURL;
    self.currentVenueName = [[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"name"];
    //infoscrollview to zero
    [cell.infoScrollView setContentOffset:CGPointZero animated:NO];
    //venue descripton
    cell.venueDescriptionTextView.text = [[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"review"];
    
    //favorite button
    cell.favoritedButton.selected = [[FavoriteJSONParser sharedSingleton].favoritedVenueArray containsObject:cell.venueURL];
    cell.favoritedButton.titleLabel.text = [NSString stringWithFormat:@"%@,%i,%i,Fav_venues,%@",cell.cityURL,self.selectedCityCell,indexPath.row,cell.venueURL];
    [cell.favoritedButton addTarget:[FavoriteJSONParser sharedSingleton] action:@selector(favoritesHeartPressed:) forControlEvents:UIControlEventTouchUpInside];
    //share button
    [cell.shareToSocialMediaButton addTarget:self action:@selector(shareButtonPressed:) forControlEvents:UIControlEventTouchDown];
    self.venueShareSentence = [[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row] objectForKey:@"tweets"];
    self.venueShareLink = [NSURL URLWithString:[NSString stringWithFormat:@"http://setmeflee.com/destinations/%@/%@",cell.cityURL,cell.venueURL]];
    
    //book flight button
    [cell.bookHotelButton addTarget:self action:@selector(bookHotelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if ([self.hotelBookingObject.mrSmithDict objectForKey:self.currentVenueCellId] ==nil && [self.hotelBookingObject.travelocityDict objectForKey:self.currentVenueCellId]==nil) {
        cell.bookHotelButton.titleLabel.text = @"WEBSITE";
    }
    
    //put venue on map
    self.venueDict = [[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:self.selectedCityCell] objectForKey:@"venues"] objectAtIndex:indexPath.row];
    MKCoordinateSpan span = MKCoordinateSpanMake(0.05, 0.05);
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake([[self.venueDict objectForKey:@"lat"] doubleValue], [[self.venueDict objectForKey:@"lng"] doubleValue]);
    MKCoordinateRegion region = MKCoordinateRegionMake(center, span);
    self.mapView.region = region;

    [self addVenueToMapView];
    
    return cell;
}

#pragma mark mapView logic
-(void)addVenueToMapView{
    VenueForMap *venueObject = [[VenueForMap alloc] init];
    venueObject.title = @"title";
    venueObject.subtitle = @"subtitle";
    venueObject.coordinate = CLLocationCoordinate2DMake([[self.venueDict objectForKey:@"lat"] doubleValue], [[self.venueDict objectForKey:@"lng"] doubleValue]);
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView addAnnotation:venueObject];
}

@end
