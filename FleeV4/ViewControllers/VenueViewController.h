//
//  VenueViewController.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-26.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface VenueViewController : UIViewController <UIScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MKMapViewDelegate>

@property (nonatomic) int selectedVenueCell;
@property (nonatomic) int venueCount;
@property (nonatomic) int selectedCityCell;
@property (strong, nonatomic) UICollectionView *venueCollectionView;
@property (strong, nonatomic) UIButton *ribbonPullDown;

@end
