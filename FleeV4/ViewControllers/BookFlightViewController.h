//
//  BookFlightViewController.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "CKCalendarView.h"

@interface BookFlightViewController : UIViewController <CKCalendarDelegate, UIWebViewDelegate>

@property (strong, nonatomic) NSString *arrivalCityAirport;
@property (weak, nonatomic) IBOutlet UILabel *cityNameBookingSentence;
@property (strong, nonatomic) NSString *cityNameBookingSentenceString;

@end
