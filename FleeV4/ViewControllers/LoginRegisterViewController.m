//
//  LoginRegisterViewController.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-21.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "LoginRegisterViewController.h"
#import "LoginModalView.h"
#import "SignUpModalView.h"
#import "LoginActualModalView.h"
#import "FleeIntro.h"

@interface LoginRegisterViewController ()

@property (weak, nonatomic) IBOutlet UILabel *getStartedLabel;
@property (weak, nonatomic) IBOutlet UILabel *fleeSlogan;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *fbLoginButton;

@end

@implementation LoginRegisterViewController

-(void)viewDidLoad{
    
    [self setupLabelDesigns];
}

#pragma mark design elements
-(void)setupLabelDesigns
{
    [self.getStartedLabel setTextColor:[UIColor colorWithRed:(210/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0f]];
    [self.getStartedLabel setFont:[UIFont fontWithName:@"FuturaStd-Medium" size:20.0f]];
    [self.fleeSlogan setTextColor:[UIColor colorWithRed:(210/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0f]];
    [self.fleeSlogan setFont:[UIFont fontWithName:@"EgyptienneFLTStd-Roman" size:15.0f]];
    self.signupButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:19.0f];
    self.loginButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:19.0f];
    self.fbLoginButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:19.0f];
    self.fbLoginButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.fbLoginButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    self.fbLoginButton.hidden = YES;
}

#pragma mark button pressed methods
- (IBAction)signUp:(id)sender {
    SignUpModalView *signUpModalView = [[SignUpModalView alloc] init];
    signUpModalView.delegate = self;
    [self.view addSubview:signUpModalView];
    [UIView animateWithDuration:0.5f animations:^{
        [signUpModalView setFrame:CGRectMake(0, 0, 768, 1024)];
    } completion:^(BOOL finished) {
    }];
}

- (IBAction)login:(id)sender {
    LoginActualModalView *logInModalView = [[LoginActualModalView alloc] init];
    logInModalView.delegate = self;
    [self.view addSubview:logInModalView];
    [UIView animateWithDuration:0.5f animations:^{
        [logInModalView setFrame:CGRectMake(0, 0, 768, 1024)];
    } completion:^(BOOL finished) {
    }];
}

- (IBAction)loginWithFB:(id)sender {
    NSLog(@"%@",NSStringFromSelector(_cmd));
}

#pragma mark delegate methods
-(void)removeSplashScreen{
    self.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark intro view
-(void)presentIntroView{
    FleeIntro *fleeIntro = [[FleeIntro alloc] init];
    [self.view addSubview:fleeIntro];
    [UIView animateWithDuration:0.5f animations:^{
        [fleeIntro setFrame:CGRectMake(0, 0, 768, 1024)];
    } completion:^(BOOL finished) {
    }];
}

@end