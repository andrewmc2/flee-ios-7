//
//  LoginRegisterViewController.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-21.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginSignUpDelegate.h"

@interface LoginRegisterViewController : UIViewController <LoginSignUpDelegate>

-(void)presentIntroView;

@end
