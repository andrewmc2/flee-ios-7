//
//  BookHotelViewController.h
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>
#import <QuartzCore/QuartzCore.h>
#import "CKCalendarView.h"

@interface BookHotelViewController : UIViewController <CKCalendarDelegate, UIWebViewDelegate>

@property (strong, nonatomic) NSString *venueID;
@property (strong, nonatomic) NSString *typeOfWebView;
@property (strong, nonatomic) NSString *urlString;
@property (weak, nonatomic) IBOutlet UILabel *hotelNameBookingSentence;
@property (weak, nonatomic) NSString *hotelNameBookingSentenceString;

@end
