//
//  BookFlightViewController.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "BookFlightViewController.h"

@interface BookFlightViewController ()

@property (weak, nonatomic) IBOutlet UIPickerView *departCity;
@property (weak, nonatomic) IBOutlet UIPickerView *numberOfDaysPicker;


@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSArray *numberOfDaysArray;

@property (strong, nonatomic) NSMutableArray *enabledDates;
@property (strong, nonatomic) NSMutableArray *enabledMonths;
@property (strong, nonatomic) CKCalendarView *calendar;
@property (weak, nonatomic) IBOutlet UIView *viewForCal;

@property (strong, nonatomic) NSDate *selectedDate;
@property (strong, nonatomic) NSString *selectedAirport;
@property (nonatomic) int selectedNumberOfDays;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (IBAction)checkFlights:(id)sender;
- (IBAction)closeModalView:(id)sender;

@end

@implementation BookFlightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.cityNameBookingSentence.text = self.cityNameBookingSentenceString;
    [self createCityObject];
    [self setupCalendars];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark setup vc

-(void)createCityObject{
    self.numberOfDaysArray = @[@1,@2,@3,@4,@5];
    self.cityArray = @[@{@"city": @"Toronto",@"airport":@"YYZ"},@{@"city": @"Vancouver",@"airport":@"YVR"},@{@"city": @"New York",@"airport":@"JFK"},@{@"city": @"San Francisco",@"airport":@"SFO"},@{@"city": @"Los Angeles",@"airport":@"LAX"},];
    self.selectedAirport = [[self.cityArray objectAtIndex:0] objectForKey:@"airport"];
    self.selectedNumberOfDays = 2;
    [self.numberOfDaysPicker selectRow:1 inComponent:0 animated:NO];
}

-(void)setupCalendars{
    self.calendar = [[CKCalendarView alloc] initWithStartDay:1 frame:CGRectMake(0, 0, 200, 200)];
    self.calendar.delegate = self;
    [self.viewForCal addSubview:self.calendar];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];
//    [self createEnabledDaysArray:[NSDate dateWithTimeIntervalSinceNow:259200]];
//    self.selectedDate = [NSDate dateWithTimeIntervalSinceNow:259200];
    [self createEnabledDaysArray:[destinationDate dateByAddingTimeInterval:345600]];
    self.selectedDate = [destinationDate dateByAddingTimeInterval:345600];
    
}

#pragma mark setup calendar

-(void)createEnabledDaysArray: (NSDate*) date
{
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.enabledDates = [NSMutableArray array];
    
    for (int i = 0; i < 60; i++) {
        NSString *dateString =  [dateFormatter stringFromDate:[date dateByAddingTimeInterval:86400*i]];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        [self.enabledDates addObject:dateFromString];
    }
    
    self.enabledMonths = [NSMutableArray array];
    for (int i = 0; i < 60; i++) {
        NSString *dateString =  [dateFormatter stringFromDate:[date dateByAddingTimeInterval:86400*i]];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        [self.enabledMonths addObject:dateFromString];
    }
    
    [self.calendar selectDate:date makeVisible:YES];
}

- (BOOL)dateIsEnabled:(NSDate *)date {
    for (NSDate *enabledDate in self.enabledDates) {
        if ([enabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)monthIsEnabled:(NSDate *)date {
    for (NSDate *enabledDate in self.enabledMonths) {
        if ([enabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    if ([self dateIsEnabled:date]) {
        //nothing
    } else {
        //        dateItem.backgroundColor = [UIColor redColor];
        dateItem.textColor = [UIColor colorWithWhite:0 alpha:0.1f];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date
{
    return [self dateIsEnabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date{
    self.selectedDate = date;
}

#pragma mark picker view delegate and data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (pickerView == self.departCity) {
        return self.cityArray.count;
    }
    
    if (pickerView == self.numberOfDaysPicker){
        return self.numberOfDaysArray.count;
    }
    
    return 0;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (pickerView == self.departCity) {
        return 187;
    }
    
    if (pickerView == self.numberOfDaysPicker){
        return 100;
    }
    
    return 0;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    if (pickerView == self.departCity) {
        return [[self.cityArray objectAtIndex:row] objectForKey:@"city"];
    }
    
    if (pickerView == self.numberOfDaysPicker){
        return [NSString stringWithFormat:@"%@",[self.numberOfDaysArray objectAtIndex:row]];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if (pickerView == self.departCity) {
        self.selectedAirport = [[self.cityArray objectAtIndex:row] objectForKey:@"airport"];
    }
    
    if (pickerView == self.numberOfDaysPicker) {
        self.selectedNumberOfDays = [[self.numberOfDaysArray objectAtIndex:row] integerValue];
    }
}

#pragma button pressed

- (IBAction)checkFlights:(id)sender {
    [self.spinner startAnimating];
    
    //date formatter
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:self.selectedDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:self.selectedDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:self.selectedDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *departDateForAPICall = [dateFormatter stringFromDate:destinationDate];
    NSString *returnDateForAPICall = [dateFormatter stringFromDate:[destinationDate dateByAddingTimeInterval:86400*self.selectedNumberOfDays]];
    NSString *urlString = [NSString stringWithFormat:@"http://setmeflee.com/flee/destinations/Nashville/flight_list/%@/%@/%@/%@/1",self.selectedAirport,self.arrivalCityAirport,departDateForAPICall,returnDateForAPICall] ;
//    NSString *urlString = @"http://setmeflee.com/flee/destinations/Nashville/flight_list/YYZ/SFO/2013-10-04/2013-10-06/1";
    [self setupWebView:urlString];

    
}

- (IBAction)closeModalView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setupWebView: (NSString*) webUrl{
    NSURL *url = [NSURL URLWithString:webUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

#pragma webview delegate 
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}

@end
