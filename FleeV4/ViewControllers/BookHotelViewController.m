//
//  BookHotelViewController.m
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import "BookHotelViewController.h"
#import "HotelBookingObject.h"

@interface BookHotelViewController ()

@property (weak, nonatomic) IBOutlet UIView *viewForCal;
@property (weak, nonatomic) IBOutlet UIPickerView *numberOfDaysPicker;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (strong, nonatomic) NSArray *cityArray;
@property (strong, nonatomic) NSArray *numberOfDaysArray;

@property (strong, nonatomic) NSMutableArray *enabledDates;
@property (strong, nonatomic) NSMutableArray *enabledMonths;
@property (strong, nonatomic) CKCalendarView *calendar;

@property (strong, nonatomic) NSDate *selectedDate;
@property (nonatomic) int selectedNumberOfDays;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

- (IBAction)closeModalView:(id)sender;
- (IBAction)checkHotels:(id)sender;

@end

@implementation BookHotelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.hotelNameBookingSentence.text = self.hotelNameBookingSentenceString;
    
    if ([self.typeOfWebView isEqualToString:@"website"]) {
        [self goToWebsite];
    }
    [self createCityObject];
    [self setupCalendars];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark setup vc
-(void)goToWebsite{
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

-(void)createCityObject{
    self.numberOfDaysArray = @[@1,@2,@3,@4,@5];
    self.selectedNumberOfDays = 2;
    [self.numberOfDaysPicker selectRow:1 inComponent:0 animated:NO];
}

-(void)setupCalendars{
    self.calendar = [[CKCalendarView alloc] initWithStartDay:1 frame:CGRectMake(0, 0, 200, 200)];
    self.calendar.delegate = self;
    [self.viewForCal addSubview:self.calendar];
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:[NSDate date]];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:[NSDate date]];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:[NSDate date]];
    //    [self createEnabledDaysArray:[NSDate dateWithTimeIntervalSinceNow:259200]];
    //    self.selectedDate = [NSDate dateWithTimeIntervalSinceNow:259200];
    [self createEnabledDaysArray:[destinationDate dateByAddingTimeInterval:345600]];
    self.selectedDate = [destinationDate dateByAddingTimeInterval:345600];
    
}

#pragma mark setup calendar

-(void)createEnabledDaysArray: (NSDate*) date
{
    NSDateFormatter *dateFormatter;
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    self.enabledDates = [NSMutableArray array];
    
    for (int i = 0; i < 60; i++) {
        NSString *dateString =  [dateFormatter stringFromDate:[date dateByAddingTimeInterval:86400*i]];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        [self.enabledDates addObject:dateFromString];
    }
    
    self.enabledMonths = [NSMutableArray array];
    for (int i = 0; i < 60; i++) {
        NSString *dateString =  [dateFormatter stringFromDate:[date dateByAddingTimeInterval:86400*i]];
        NSDate *dateFromString = [dateFormatter dateFromString:dateString];
        [self.enabledMonths addObject:dateFromString];
    }
    
    [self.calendar selectDate:date makeVisible:YES];
}

- (BOOL)dateIsEnabled:(NSDate *)date {
    for (NSDate *enabledDate in self.enabledDates) {
        if ([enabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)monthIsEnabled:(NSDate *)date {
    for (NSDate *enabledDate in self.enabledMonths) {
        if ([enabledDate isEqualToDate:date]) {
            return YES;
        }
    }
    return NO;
}

- (void)calendar:(CKCalendarView *)calendar configureDateItem:(CKDateItem *)dateItem forDate:(NSDate *)date
{
    if ([self dateIsEnabled:date]) {
        //nothing
    } else {
        //        dateItem.backgroundColor = [UIColor redColor];
        dateItem.textColor = [UIColor colorWithWhite:0 alpha:0.1f];
    }
}

- (BOOL)calendar:(CKCalendarView *)calendar willSelectDate:(NSDate *)date
{
    return [self dateIsEnabled:date];
}

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date{
    self.selectedDate = date;
}


#pragma mark picker view delegate and data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{

    return self.numberOfDaysArray.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    
    return 100;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    return [NSString stringWithFormat:@"%@",[self.numberOfDaysArray objectAtIndex:row]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
        self.selectedNumberOfDays = [[self.numberOfDaysArray objectAtIndex:row] integerValue];
}

- (IBAction)closeModalView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)checkHotels:(id)sender {
    //date formatter
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:self.selectedDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:self.selectedDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:interval sinceDate:self.selectedDate];
    NSDate *returnDate = [NSDate dateWithTimeInterval:86400*self.selectedNumberOfDays sinceDate:destinationDate];
    NSURL *url = [NSURL URLWithString:[self setupHotelUrlString:destinationDate withReturnDate:returnDate]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

#pragma mark setup strings

-(NSString*)setupHotelUrlString: (NSDate*) departDate
                 withReturnDate: (NSDate*) returnDate{
    
    [self.spinner startAnimating];
    
    HotelBookingObject *hotelBookingObject = [[HotelBookingObject alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    if ([self.typeOfWebView isEqualToString:@"mrsmith"]) {
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        //set mr smith id
        NSString *mrSmithId = [hotelBookingObject.mrSmithDict objectForKey:self.venueID];
        
        return [NSString stringWithFormat:@"http://www.mrandmrssmith.com/book/step-1?book_hotel_id=%@&date_from=%@&nights=%i&adults=1&affID=flee",mrSmithId,[dateFormatter stringFromDate:departDate],self.selectedNumberOfDays];
    }
    
    if ([self.typeOfWebView isEqualToString:@"travelocity"]) {
        [dateFormatter setDateFormat:@"MM"];
        NSString *departMonth = [dateFormatter stringFromDate:departDate];
        NSString *returnMonth = [dateFormatter stringFromDate:returnDate];
        [dateFormatter setDateFormat:@"dd"];
        NSString *departDay = [dateFormatter stringFromDate:departDate];
        NSString *returnDay = [dateFormatter stringFromDate:returnDate];
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *departYear = [dateFormatter stringFromDate:departDate];
        NSString *returnYear = [dateFormatter stringFromDate:returnDate];
        
        NSString *travelocityId = [hotelBookingObject.travelocityDict objectForKey:self.venueID];
        
        return [NSString stringWithFormat:@"http://www.res99.com/nexres/reservations/availability.cgi?hotels_id=%@&doa_mm=%@&doa_dd=%@&doa_yy=%@&dod_mm=%@&dod_dd=%@&dod_yy=%@&lrp=334d0a&num_rooms=1&num_adults=1",travelocityId,departMonth,departDay,departYear,returnMonth,returnDay,returnYear];
    }

    return nil;
}

#pragma webview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}
@end
