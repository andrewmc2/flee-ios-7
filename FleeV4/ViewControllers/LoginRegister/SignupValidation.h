//
//  SignupValidation.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SignupValidation : NSObject

-(NSMutableArray *)validateSignUp: (NSString *) name
                     validateEmail: (NSString*) email
          validatePassword: (NSString *) password
   validateConfirmPassword: (NSString *) confirmedPassword;

-(NSMutableArray *)validateLogIn:(NSString *)email
           validateLoginPassword:(NSString *)password;
@end
