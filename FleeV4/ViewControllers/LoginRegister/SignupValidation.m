//
//  SignupValidation.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "SignupValidation.h"

@interface SignupValidation ()

- (BOOL) validateName: (NSString*) candidate;

- (BOOL) validateEmail: (NSString *) candidate;

@end

@implementation SignupValidation

- (BOOL) validateName: (NSString*) candidate {
    NSString *nameRegex = @"[A-Za-z-]{5,200}";
    NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [nameTest evaluateWithObject:candidate];
}

- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

-(NSMutableArray *)validateSignUp:(NSString *)name validateEmail:(NSString *)email validatePassword:(NSString *)password validateConfirmPassword:(NSString *)confirmedPassword{
    
    NSMutableArray *validationStrings = [NSMutableArray arrayWithObjects:@"",@"",@"",@"", nil];
    
    int arrayCount = 0;
    
    if (![self validateName:name]) {
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"name"];
        arrayCount++;
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }
    
    if(![self validateEmail:email]){
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"email"];
        arrayCount++;
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }
    
    if (password.length < 5) {
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"password"];
        arrayCount++;
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }
    
    if (![password isEqualToString:confirmedPassword]) {
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"confirm password"];
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }

    return validationStrings;
}

-(NSMutableArray *)validateLogIn:(NSString *)email validateLoginPassword:(NSString *)password{
    NSMutableArray *validationStrings = [NSMutableArray arrayWithObjects:@"",@"",@"",@"", nil];
    
    int arrayCount = 0;
    
    if(![self validateEmail:email]){
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"email"];
        arrayCount++;
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }
    
    if (password.length < 5) {
        [validationStrings replaceObjectAtIndex:arrayCount withObject:@"password"];
    } else {
        [validationStrings removeObjectAtIndex:arrayCount];
    }
    
    return validationStrings;
}
@end