//
//  CityAllJSONParser.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-28.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityAllJSONParser : NSObject

+(CityAllJSONParser*)sharedSingleton;

@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) NSMutableArray *cityAllJSON;

@end
