//
//  SearchJSONParser.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-24.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "SearchJSONParser.h"
#import <AFNetworking.h>

@implementation SearchJSONParser

-(id)init
{
    self = [super init];
    
    if (self) {
        //do more customization if needed
        NSString *searchAPIString = @"http://setmeflee.com/flee/api/recommend/YYZ/2013-10-20/2013-11-01/0/0/0/0/0/1";
        NSURL *url = [NSURL URLWithString:searchAPIString];
        self.request = [NSURLRequest requestWithURL:url];
    }
    
    return self;
}

+(SearchJSONParser*)sharedSingleton
{
    static SearchJSONParser *sharedSingleton;
    if (!sharedSingleton) {
        sharedSingleton = [[SearchJSONParser alloc] init];
    }
    return sharedSingleton;
}

-(NSMutableArray*)createCityAllArray{
    NSMutableArray *cityArray = [NSMutableArray array];
    
    for (int i = 0; i < self.cityAllJSON.count; i++) {
        NSString *cityURL = [[self.cityAllJSON objectAtIndex:i] objectForKey:@"URL"];
        [cityArray addObject:cityURL];
    }
    
    return cityArray;
}

@end