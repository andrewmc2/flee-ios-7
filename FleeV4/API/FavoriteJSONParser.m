//
//  FavoriteJSONParser.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "FavoriteJSONParser.h"
#import "API.h"
#import "SearchJSONParser.h"

@implementation FavoriteJSONParser

-(id)init
{
    self = [super init];
    
    if (self) {
        //do more customization if needed
    }
    
    return self;
}

+(FavoriteJSONParser*)sharedSingleton
{
    static FavoriteJSONParser *sharedSingleton;
    if (!sharedSingleton) {
        sharedSingleton = [[FavoriteJSONParser alloc] init];
    }
    return sharedSingleton;
}

#pragma mark city methods

-(void)createFavoritedCityandVenueArrays{
    self.favoritedCityArray = [NSMutableArray array];
    self.favoritedVenueArray = [NSMutableArray array];
    
    for (NSString *favoriteCityResult in ((NSArray*)[self.user objectForKey:@"Fav_cities"])) {
        [self.favoritedCityArray addObject:[(NSDictionary*)favoriteCityResult objectForKey:@"URL"]];
    }
    
    for (NSString *favoriteVenueResult in ((NSArray*)[self.user objectForKey:@"Fav_venues"])) {
        [self.favoritedVenueArray addObject:[(NSDictionary*)favoriteVenueResult objectForKey:@"id"]];
    }
    
}

-(BOOL)checkCityIsFavorited: (NSString*)cityName
{
    return [self.favoritedCityArray containsObject:cityName];
}

-(void)loadCityArray: (NSMutableArray*)cityArray
{
    self.favoritedCityArray = [NSMutableArray arrayWithArray:cityArray];
}

-(void)popArray: (int)arrayIndex
    typeOfArray: (NSString*)cityOrVenue
{
    if ([cityOrVenue isEqualToString:@"Fav_cities"]) {
        [self.favoritedCityArray removeObjectAtIndex:arrayIndex];
    }
    
    if ([cityOrVenue isEqualToString:@"Fav_venues"]) {
        [self.favoritedVenueArray removeObjectAtIndex:arrayIndex];
    }
}

-(IBAction)favoritesHeartPressed:(UIButton *)sender{
    //create array out of titleLabel string
    NSArray *titleLabelInfoArray = [sender.titleLabel.text componentsSeparatedByString:@","];
    
    if(sender.selected) {
        if ([[titleLabelInfoArray objectAtIndex:3] isEqualToString:@"Fav_cities"]) {
            //update array and heart label
            int cityIndexInFavoriteArray = [self.favoritedCityArray indexOfObject:[titleLabelInfoArray objectAtIndex:0]];
            [self.favoritedCityArray removeObject:[titleLabelInfoArray objectAtIndex:0]];
            sender.selected = NO;
            //update json
            NSMutableArray *favCityArrayFromUserJSON = [NSMutableArray arrayWithArray:[self.user objectForKey:[titleLabelInfoArray objectAtIndex:3]]];
            [favCityArrayFromUserJSON removeObjectAtIndex:cityIndexInFavoriteArray];
            [self.user setObject:favCityArrayFromUserJSON forKey:[titleLabelInfoArray objectAtIndex:3]];
            //delete on server
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self.user objectForKey:@"username"], @"username", [titleLabelInfoArray objectAtIndex:0], @"cityname", [titleLabelInfoArray objectAtIndex:4], @"venue_id", nil];
            [[API sharedInstance] deleteFavorite:params onCompletion:^(NSDictionary *json) {
                NSLog(@"deleted");
                [self.delegate reloadCollectionViews];
            }];
        } else {
            //update array and heart label
            int venueIndexInFavoriteArray = [self.favoritedVenueArray indexOfObject:[titleLabelInfoArray objectAtIndex:4]];
            [self.favoritedVenueArray removeObject:[titleLabelInfoArray objectAtIndex:4]];
            sender.selected = NO;
            //update json
            NSMutableArray *favVenueArrayFromUserJSON = [NSMutableArray arrayWithArray:[self.user objectForKey:[titleLabelInfoArray objectAtIndex:3]]];
            [favVenueArrayFromUserJSON removeObjectAtIndex:venueIndexInFavoriteArray];
            [self.user setObject:favVenueArrayFromUserJSON forKey:[titleLabelInfoArray objectAtIndex:3]];
            //add on server
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self.user objectForKey:@"username"], @"username", [titleLabelInfoArray objectAtIndex:0], @"cityname", [titleLabelInfoArray objectAtIndex:4], @"venue_id", nil];
            [[API sharedInstance] deleteFavorite:params onCompletion:^(NSDictionary *json) {
                NSLog(@"deleted");
                [self.delegate reloadCollectionViews];
            }];
        }
    } else {
        if ([[titleLabelInfoArray objectAtIndex:3] isEqualToString:@"Fav_cities"]) {
            //update array and heart label
            [self.favoritedCityArray addObject:[titleLabelInfoArray objectAtIndex:0]];
            sender.selected = YES;
            //update json
            NSMutableArray *favCityArrayFromUserJSON = [NSMutableArray arrayWithArray:[self.user objectForKey:[titleLabelInfoArray objectAtIndex:3]]];
            [favCityArrayFromUserJSON addObject:[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:[[titleLabelInfoArray objectAtIndex:2] integerValue]]];
            [self.user setObject:favCityArrayFromUserJSON forKey:[titleLabelInfoArray objectAtIndex:3]];
            //add on server
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self.user objectForKey:@"username"], @"username", [titleLabelInfoArray objectAtIndex:0], @"cityname", [titleLabelInfoArray objectAtIndex:4], @"venue_id", nil];
            [[API sharedInstance] addFavorite:params onCompletion:^(NSDictionary *json) {
                NSLog(@"added");
                [self.delegate reloadCollectionViews];
            }];
        } else {
            //0 city url 1 city array position 2 venue array position 3 Fav_venues 4 venue id
            
            //update array and heart label with venue id
            [self.favoritedVenueArray addObject:[titleLabelInfoArray objectAtIndex:4]];
            sender.selected = YES;
            //update json with new venue
            //create venue array that will replace old one
            NSMutableArray *favVenueArrayFromUserJSON = [NSMutableArray arrayWithArray:[self.user objectForKey:[titleLabelInfoArray objectAtIndex:3]]];
            //add the new venue to that array
            [favVenueArrayFromUserJSON addObject:[[[[SearchJSONParser sharedSingleton].citySearchJSON objectAtIndex:[[titleLabelInfoArray objectAtIndex:1] integerValue]] objectForKey:@"venues"] objectAtIndex:[[titleLabelInfoArray objectAtIndex:2] integerValue]]];
            [self.user setObject:favVenueArrayFromUserJSON forKey:[titleLabelInfoArray objectAtIndex:3]];
            //add on server
            NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:[self.user objectForKey:@"username"], @"username", [titleLabelInfoArray objectAtIndex:0], @"cityname", [titleLabelInfoArray objectAtIndex:4], @"venue_id", nil];
            [[API sharedInstance] addFavorite:params onCompletion:^(NSDictionary *json) {
                NSLog(@"added");
                [self.delegate reloadCollectionViews];
            }];
        }
    }
}

@end
