//
//  API.m
//  fleeIOS6v1
//
//  Created by Andrew McCallum14 on 2013-07-17.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "API.h"

//the web location of the service
#define kAPIHost @"http://setmeflee.com/flee/api/"
#define kAPIPath @"signin/"

@implementation API

@synthesize user;

#pragma mark - Singleton methods

+(API*)sharedInstance
{
    static API *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:kAPIHost]];
    });
    
    return sharedInstance;
}

-(API*)init
{
    //call super init
    self = [super init];
    
    if (self != nil) {
        //initialize the object
        user = nil;
        
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        
        //accetp http header; see http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html#sec14.1
        [self setDefaultHeader:@"Accept" value:@"application/json"];
    }
    
    return self;
}

-(BOOL)isAuthorized
{
    if ([[self.user objectForKey:@"code"] integerValue] >0) {
        return YES;
    } else {
        return 0;
    }
}

-(void)loginWithParams:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock
{
    //this sends the parameters via POST
    NSMutableURLRequest *apiRequest =
    [self multipartFormRequestWithMethod:@"POST"
                                    path:@"http://setmeflee.com/flee/api/login"
                              parameters:params
               constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                   //TODO: attach file if needed
               }];
    
    //create operation to handle network communication in bg and initialize it with POST request you prepared
    AFJSONRequestOperation* operation = [[AFJSONRequestOperation alloc] initWithRequest: apiRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //success!
        completionBlock(responseObject);
        NSLog(@"success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //failure :(
        completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
    }];
    
    [operation start];
}

-(void)registerUser:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock
{
    //this sends the parameters via POST
    NSMutableURLRequest *apiRequest =
    [self multipartFormRequestWithMethod:@"POST"
                                    path:@"http://setmeflee.com/flee/api/signup"
                              parameters:params
               constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                   //TODO: attach file if needed
               }];
    
    //create operation to handle network communication in bg and initialize it with POST request you prepared
    AFJSONRequestOperation* operation = [[AFJSONRequestOperation alloc] initWithRequest: apiRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //success!
        completionBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //failure :(
        completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
    }];
    
    [operation start];
}
    
-(void)deleteFavorite:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock
{
    //this sends the parameters via POST
    NSMutableURLRequest *apiRequest =
    [self multipartFormRequestWithMethod:@"POST"
                                    path:@"http://setmeflee.com/flee/api/delete_fav"
                              parameters:params
               constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                   //TODO: attach file if needed
               }];
    
    //create operation to handle network communication in bg and initialize it with POST request you prepared
    AFJSONRequestOperation* operation = [[AFJSONRequestOperation alloc] initWithRequest: apiRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //success!
        completionBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //failure :(
        completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
    }];
    
    [operation start];
}

-(void)addFavorite:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock;
{
    //this sends the parameters via POST
    NSMutableURLRequest *apiRequest =
    [self multipartFormRequestWithMethod:@"POST"
                                    path:@"http://setmeflee.com/flee/api/insert_fav"
                              parameters:params
               constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
                   //TODO: attach file if needed
               }];
    
    //create operation to handle network communication in bg and initialize it with POST request you prepared
    AFJSONRequestOperation* operation = [[AFJSONRequestOperation alloc] initWithRequest: apiRequest];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        //success!
        completionBlock(responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //failure :(
        completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
    }];
    
    [operation start];
}

@end
