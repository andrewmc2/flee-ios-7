//
//  FavoriteJSONParser.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RefreshFavoriteCollectionViewDelegate.h"

@interface FavoriteJSONParser : NSObject

@property (strong, nonatomic) NSMutableArray *favoritedCityArray;
@property (strong, nonatomic) NSMutableArray *favoritedVenueArray;
@property (strong, nonatomic) NSMutableDictionary *user;
@property (weak, nonatomic) id <ReloadCollectionViewsDelegate> delegate;

+(FavoriteJSONParser*)sharedSingleton;

//city methods
-(void)createFavoritedCityandVenueArrays;
-(BOOL)checkCityIsFavorited: (NSString*)cityName;
-(void)loadCityArray: (NSArray*)cityArray;
-(void)popArray: (int)arrayIndex
    typeOfArray: (NSString*)cityOrVenue;


//methods for all favorite pressed except for favorites collection view
-(IBAction)favoritesHeartPressed:(UIButton*)sender;

@end
