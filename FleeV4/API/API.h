//
//  API.h
//  fleeIOS6v1
//
//  Created by Andrew McCallum14 on 2013-07-17.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "AFHTTPClient.h"
#import "AFNetworking.h"

//api call completion block with result as json
typedef void (^JSONResponseBlock)(NSDictionary* json);

@interface API : AFHTTPClient

//the authorized user: holds user data once it has been authenticated
@property (strong, nonatomic) NSDictionary *user;

//singleton class definition
+(API*)sharedInstance;

//check if user is authorized
-(BOOL)isAuthorized;

//send API command to server
-(void)loginWithParams: (NSMutableDictionary*)params onCompletion:(JSONResponseBlock)completionBlock;

//register user
-(void)registerUser:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock;

//delete favorite
-(void)deleteFavorite:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock;

//add favorite
-(void)addFavorite:(NSMutableDictionary *)params onCompletion:(JSONResponseBlock)completionBlock;


@end
