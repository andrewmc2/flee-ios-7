//
//  CityAllJSONParser.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-28.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "CityAllJSONParser.h"
#import <AFNetworking.h>

@implementation CityAllJSONParser

-(id)init
{
    self = [super init];
    
    if (self) {
        //do more customization if needed
        NSString *searchAPIString = @"http://setmeflee.com/flee/api/all";
        NSURL *url = [NSURL URLWithString:searchAPIString];
        self.request = [NSURLRequest requestWithURL:url];
    }
    
    return self;
}

+(CityAllJSONParser*)sharedSingleton
{
    static CityAllJSONParser *sharedSingleton;
    if (!sharedSingleton) {
        sharedSingleton = [[CityAllJSONParser alloc] init];
    }
    return sharedSingleton;
}

@end