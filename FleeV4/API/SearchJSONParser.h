//
//  SearchJSONParser.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-24.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchJSONParser : NSObject

+(SearchJSONParser*)sharedSingleton;

@property (strong, nonatomic) NSMutableArray *citySearchJSON;
@property (strong, nonatomic) NSURLRequest *request;
@property (strong, nonatomic) NSMutableArray *cityAllJSON;

-(NSMutableArray*)createCityAllArray;

@end
