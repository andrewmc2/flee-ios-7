//
//  FirstLaunchCityViewControllerModal.m
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import "FirstLaunchCityViewControllerModal.h"

@implementation FirstLaunchCityViewControllerModal

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.624 alpha:0.7f];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
        [imageView setImage:[UIImage imageNamed:@"FirstLaunchCityVC"]];
        [self addSubview:imageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
