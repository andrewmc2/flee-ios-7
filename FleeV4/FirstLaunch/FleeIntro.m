//
//  FleeIntro.m
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import "FleeIntro.h"

@implementation FleeIntro

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
        [imageView setImage:[UIImage imageNamed:@"FleeFirstLaunch"]];
        [self addSubview:imageView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
