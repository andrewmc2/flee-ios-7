//
//  FirstLaunchSingleton.m
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import "FirstLaunchSingleton.h"

@implementation FirstLaunchSingleton

-(id)init
{
    self = [super init];
    
    if (self) {
        //do more customization if needed
    }
    
    return self;
}

+(FirstLaunchSingleton*)sharedSingleton
{
    static FirstLaunchSingleton *sharedSingleton;
    if (!sharedSingleton) {
        sharedSingleton = [[FirstLaunchSingleton alloc] init];
    }
    return sharedSingleton;
}

@end
