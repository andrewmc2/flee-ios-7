//
//  FleeIntroView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-29.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "FirstLaunchModal.h"

@implementation FirstLaunchModal

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 1024, 768, 1024);
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.624 alpha:0.9f];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeModalViewSelector:)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}

-(IBAction)closeModalViewSelector:(id)sender
{
    [UIView animateWithDuration:0.5f animations:^{
        [self setFrame:CGRectMake(0, 1004, 768, 1004)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
