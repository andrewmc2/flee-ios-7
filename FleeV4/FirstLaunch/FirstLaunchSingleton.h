//
//  FirstLaunchSingleton.h
//  
//
//  Created by Andrew McCallum14 on 2013-09-29.
//
//

#import <Foundation/Foundation.h>

@interface FirstLaunchSingleton : NSObject

+(FirstLaunchSingleton*)sharedSingleton;

@property (nonatomic) BOOL loginRegisterViewControllerFirstLaunch;
@property (nonatomic) BOOL mainViewControllerFirstLaunch;
@property (nonatomic) BOOL cityViewControllerFirstLaunch;
@property (nonatomic) BOOL venueViewControllerFirstLaunch;

@end
