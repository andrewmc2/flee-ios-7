//
//  HotelBookingObject.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "HotelBookingObject.h"

@implementation HotelBookingObject

- (id)init
{
    self = [super init];
    if (self) {
        NSArray *mrSmithKeys = @[@"1",@"2",@"108",@"104",@"119",@"160",@"161",@"162",@"175",@"211",@"307"];
        NSArray *mrSmithObjects = @[@"897", @"895",@"1611",@"1622",@"381",@"1654",@"1652",@"1666",@"1056",@"659",@"409"];
        self.mrSmithDict = [NSDictionary dictionaryWithObjects:mrSmithObjects forKeys:mrSmithKeys];
        
        NSArray *travelocityKeys = @[@"4",@"22",@"23",@"24",@"37",@"48",@"49",@"52",@"91",@"92",@"120",@"121",@"140",@"177",@"351",@"198",@"175",@"213",@"232",@"233",@"234",@"235",@"251",@"252",@"271",@"286",@"288",@"308",@"329",@"330",@"332",@"331",@"349",@"350",@"351"];
        NSArray *travelocityObjects = @[@"10230283",@"11389798",@"11281687",@"10229724",@"10208075",@"11407184",@"11291262",@"11340150",@"11387701",@"11298075",@"11297561",@"11281694",@"10059901",@"11425616",@"11399909",@"11306727",@"11277922",@"11255764",@"10205686",@"10045790",@"10054817",@"11259144",@"10190914",@"11321384",@"11324220",@"11422404",@"11242558",@"11411952",@"11389583",@"10216475",@"11364475",@"11364475",@"10015994",@"10205548",@"11331203"];
        self.travelocityDict = [NSDictionary dictionaryWithObjects:travelocityObjects forKeys:travelocityKeys];
    }
    return self;
}

@end
