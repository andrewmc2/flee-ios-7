//
//  HotelBookingObject.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HotelBookingObject : NSObject

@property (strong, nonatomic) NSDictionary *mrSmithDict;
@property (strong, nonatomic) NSDictionary *travelocityDict;

@end
