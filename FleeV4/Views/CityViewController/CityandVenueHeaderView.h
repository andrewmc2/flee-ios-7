//
//  CityandVenueHeaderView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityandVenueHeaderView : UIView <UIScrollViewDelegate>

@property (strong, nonatomic) UIPageControl *pageControlForOptions;
@property (strong, nonatomic) UIButton *logoButton;
@property (strong,nonatomic) UIButton *logOutButton;

@end
