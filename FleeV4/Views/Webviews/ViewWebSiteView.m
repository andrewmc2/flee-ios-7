//
//  ViewWebSiteView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-29.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "ViewWebSiteView.h"


@implementation ViewWebSiteView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 100, 768, 924)];
        self.webView.delegate = self;
        
        self.spinner = [[UIActivityIndicatorView alloc] init];
        self.spinner.color = [UIColor blackColor];
        self.spinner.frame = CGRectMake(379, 500, 20, 20);
        self.spinner.hidesWhenStopped = YES;
    }
    return self;
}

-(void)setupWebView{
    [self.spinner startAnimating];
    NSURL *url = [NSURL URLWithString:self.urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self addSubview:self.webView];
    [self addSubview:self.spinner];
    [self.webView loadRequest:request];
}

#pragma webview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.spinner stopAnimating];
}

@end
