//
//  BookHotelModalView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "BookHotelModalView.h"

@interface BookHotelModalView ()

@property (strong, nonatomic) UIWebView *webView;

@end

@implementation BookHotelModalView

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 1024, 768, 1024);
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.624 alpha:0.8f];
        self.closeModalView = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeModalView addTarget:self
                                action:@selector(closeModalView:)
                      forControlEvents:UIControlEventTouchDown];
        self.closeModalView.frame = CGRectMake(654.0, 56.5, 25.0, 25.0);
        [self.closeModalView setImage:[UIImage imageNamed:@"closeModalX.png"] forState:UIControlStateNormal];
        [self addSubview:self.closeModalView];
    }
    return self;
}

-(IBAction)closeModalView:(id)sender
{
    [UIView animateWithDuration:0.5f animations:^{
        [self setFrame:CGRectMake(0, 1004, 768, 1004)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)setupMrSmithString: (NSString*) venueId
           withDateString: (NSString*) checkInDate{
    self.urlStringForWebView = [NSString stringWithFormat:@"http://www.mrandmrssmith.com/book/step-1?book_hotel_id=%@&date_from=%@&nights=2&adults=1&affID=flee",venueId,checkInDate];
    [self addWebViewToModal:self.urlStringForWebView];
}

-(void)setupTravelocityString: (NSString*) venueId
           withDateString: (NSString*) checkInDate{
    self.urlStringForWebView = [NSString stringWithFormat:@"http://www.res99.com/nexres/reservations/availability.cgi?hotels_id=%@&doa_mm=10&doa_dd=15&doa_yy=2013&dod_mm=10&dod_dd=17&dod_yy=2013&lrp=334d0a&num_rooms=1&num_adults=1",venueId];
    [self addWebViewToModal:self.urlStringForWebView];
}

-(void)addWebViewToModal: (NSString*) webUrl{
    //add webview
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 180, 768, 800)];
    NSURL *url = [NSURL URLWithString:webUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self addSubview:self.webView];
    [self.webView loadRequest:request];
}

@end