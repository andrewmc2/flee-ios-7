//
//  FlightBookingModalView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "FlightBookingModalView.h"

@interface FlightBookingModalView ()
@property (strong, nonatomic) UIWebView *webView;
@end

@implementation FlightBookingModalView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 1024, 768, 1024);
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.624 alpha:1.0f];
        self.closeModalView = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeModalView addTarget:self
                                action:@selector(closeModalView:)
                      forControlEvents:UIControlEventTouchDown];
        self.closeModalView.frame = CGRectMake(654.0, 56.5, 25.0, 25.0);
        [self.closeModalView setImage:[UIImage imageNamed:@"closeModalX.png"] forState:UIControlStateNormal];
        [self addSubview:self.closeModalView];
    }
    return self;
}

-(IBAction)departDatePickerChanged:(id)sender{
    self.datePickerReturn.minimumDate = [NSDate dateWithTimeInterval:259200 sinceDate:self.datePickerDepart.date];
    self.datePickerReturn.maximumDate = [NSDate dateWithTimeInterval:2592000 sinceDate:self.datePickerDepart.date];
    self.datePickerReturn.date = self.datePickerReturn.minimumDate;
}

-(IBAction)closeModalView:(id)sender
{
    [UIView animateWithDuration:0.5f animations:^{
        [self setFrame:CGRectMake(0, 1004, 768, 1004)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)addWebViewToModal: (NSString*) webUrl{
    //add webview
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 180, 768, 800)];
    NSURL *url = [NSURL URLWithString:webUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self addSubview:self.webView];
    [self.webView loadRequest:request];
}

//@"http://setmeflee.com/flee/destinations/Nashville/flight_list/YYZ/BNA/2013-10-18/2013-10-22/1"

@end
