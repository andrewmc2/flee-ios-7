//
//  BookHotelModalView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookHotelModalView : UIView

@property (nonatomic, weak) UIButton *closeModalView;
@property (nonatomic, weak) NSString *urlStringForWebView;
//mr smith
@property (nonatomic,strong) NSString *venueId;
@property (nonatomic,strong) NSString *checkInDate;


-(void)setupMrSmithString: (NSString*) venueId
           withDateString: (NSString*) leaveDate;

-(void)setupTravelocityString: (NSString*) venueId
               withDateString: (NSString*) checkInDate;
-(void)addWebViewToModal: (NSString*) webUrl;
@end
