//
//  ViewWebSiteView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-29.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "LoginModalView.h"

@interface ViewWebSiteView : LoginModalView <UIWebViewDelegate>

@property (strong, nonatomic) NSString *urlString;
@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

-(void)setupWebView;

@end
