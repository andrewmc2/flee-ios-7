//
//  FlightBookingModalView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-27.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FlightBookingModalView : UIView

@property (nonatomic, weak) UIButton *closeModalView;
@property (nonatomic, weak) NSString *urlStringForWebView;
@property (strong, nonatomic) UIDatePicker *datePickerDepart;
@property (strong, nonatomic) UIDatePicker *datePickerReturn;
@property (strong, nonatomic) UIPickerView *departingCity;

-(void)addWebViewToModal: (NSString*) webUrl;
@end
