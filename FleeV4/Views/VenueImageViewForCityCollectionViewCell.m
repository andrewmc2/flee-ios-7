//
//  VenueImageViewForCityCollectionViewCell.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "VenueImageViewForCityCollectionViewCell.h"

@implementation VenueImageViewForCityCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.venueLabel = [[UILabel alloc] initWithFrame:CGRectMake(85, 85, 550, 50)];
        self.venueLabel.textColor = [UIColor whiteColor];
        self.venueLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:37.5f];
        self.venueLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.venueLabel];
        self.userInteractionEnabled = YES;
        self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
        self.tapGestureRecognizer.numberOfTapsRequired = 1;
        [self addGestureRecognizer:self.tapGestureRecognizer];
    }
    return self;
}

@end
