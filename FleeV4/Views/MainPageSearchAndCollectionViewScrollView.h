//
//  MainPageSearchAndCollectionViewScrollView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainPageSearchAndCollectionViewScrollView : UIScrollView

@property (strong, nonatomic) UICollectionView *mainPageCollectionView;
@property (strong, nonatomic) UICollectionView *favoritesCollectionView;

@end
