//
//  LoginModalView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-22.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "LoginModalView.h"

@implementation LoginModalView

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 1024, 768, 1024);
        self.opaque = NO;
        self.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.624 alpha:0.8f];
        self.closeModalView = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.closeModalView addTarget:self
                           action:@selector(closeModalView:)
                 forControlEvents:UIControlEventTouchDown];
        self.closeModalView.frame = CGRectMake(654.0, 56.5, 25.0, 25.0);
        [self.closeModalView setImage:[UIImage imageNamed:@"closeModalX.png"] forState:UIControlStateNormal];
        [self addSubview:self.closeModalView];
    }
    return self;
}

-(IBAction)closeModalView:(id)sender
{
    [UIView animateWithDuration:0.5f animations:^{
        [self setFrame:CGRectMake(0, 1004, 768, 1004)];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
