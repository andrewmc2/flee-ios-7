//
//  SearchIconView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-29.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "SearchIconView.h"

@implementation SearchIconView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 0, 768, 130);
        
        int spaceBetweenIcons = 58;
        for (int i = 0; i < 6; i++) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(spaceBetweenIcons, 35, 60, 60)];
            imageView.alpha = 0.3f;
            [imageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"searchIcon%i",i+1]]];
            [self addSubview:imageView];
            spaceBetweenIcons += 58+60;
        }
        
        //add label
        UILabel *underconstructionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, 768, 60)];
        underconstructionLabel.text = @"SEARCH COMING SOON";
        underconstructionLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        underconstructionLabel.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        underconstructionLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:underconstructionLabel];
    }
    return self;
}

@end
