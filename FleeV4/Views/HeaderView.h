//
//  HeaderView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-21.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView <UIScrollViewDelegate>

@property (strong, nonatomic) UIPageControl *pageControlForOptions;
@property (strong, nonatomic) UIButton *ribbonPullDown;
@property (strong,nonatomic) UIButton *logOutButton;

@end
