//
//  SignUpModalView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-22.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "SignUpModalView.h"
#import "SignupValidation.h"
#import "NSString+MD5.h"
#import "API.h"
#import <UICKeyChainStore.h>

@interface SignUpModalView ()

@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *emailTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UITextField *confirmPasswordTextField;
@property (strong, nonatomic) UILabel *validationErrorMessage;
@property (strong, nonatomic) NSMutableArray *validationResultsArray;
@property (strong,nonatomic) UIButton *signUpButtonModal;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end

@implementation SignUpModalView

- (id)init
{
    self = [super init];
    if (self) {
        
        //add label for textfields
        NSArray *labelNameArray = @[@"Name",@"Email Address",@"Password",@"Confirm Password"];
        int labelYValue = 210;
        for (int i = 0; i < 4; i++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(134, labelYValue, 300, 45)];
            label.font = [UIFont fontWithName:@"FuturaStd-Medium" size:18.0f];
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
            label.text = [labelNameArray objectAtIndex:i];
            [self addSubview:label];
            labelYValue += 49;
        }
        
        //add textfields
        self.nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 214, 318.5, 35)];
        self.nameTextField.backgroundColor = [UIColor whiteColor];
        self.nameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.nameTextField.autocorrectionType = FALSE;
        self.nameTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.nameTextField.delegate = self;
        [self.nameTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:self.nameTextField];
        
        self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 263, 318.5, 35)];
        self.emailTextField.backgroundColor = [UIColor whiteColor];
        self.emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.emailTextField.autocorrectionType = FALSE;
        self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.emailTextField.delegate = self;
        [self.emailTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:self.emailTextField];
        
        self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 312, 318.5, 35)];
        self.passwordTextField.backgroundColor = [UIColor whiteColor];
        self.passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.passwordTextField.autocorrectionType = FALSE;
        self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.passwordTextField.delegate = self;
        [self.passwordTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        self.passwordTextField.secureTextEntry = YES;
        [self addSubview:self.passwordTextField];
        
        self.confirmPasswordTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 361, 318.5, 35)];
        self.confirmPasswordTextField.backgroundColor = [UIColor whiteColor];
        self.confirmPasswordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.confirmPasswordTextField.autocorrectionType = FALSE;
        self.confirmPasswordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.confirmPasswordTextField.delegate = self;
        [self.confirmPasswordTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        self.confirmPasswordTextField.secureTextEntry = YES;
        [self addSubview:self.confirmPasswordTextField];
        
        //validation error message
        self.validationErrorMessage = [[UILabel alloc]initWithFrame:CGRectMake(134, 165, 500, 35)];
        self.validationErrorMessage.text = @"";
        [self.validationErrorMessage setTextColor:[UIColor whiteColor]];
        self.validationErrorMessage.backgroundColor = [UIColor clearColor];
        [self.validationErrorMessage setFont:[UIFont fontWithName:@"FuturaStd-Medium" size:18.0f]];
        [self addSubview:self.validationErrorMessage];
        
        //sign up button
        self.signUpButtonModal = [UIButton buttonWithType:UIButtonTypeCustom];
        self.signUpButtonModal.frame = CGRectMake(509, 420, 125, 33.5);
        [self.signUpButtonModal setBackgroundImage:[UIImage imageNamed:@"signupLoginButtonBG.png"]  forState:UIControlStateNormal];
        [self.signUpButtonModal setBackgroundImage:[UIImage imageNamed:@"signUpLoginButtonHighlitedBG.png"] forState:UIControlStateHighlighted];
        [self.signUpButtonModal.titleLabel setTextColor:[UIColor whiteColor]];
        self.signUpButtonModal.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:19];
        [self.signUpButtonModal setTitle:@"SIGN UP" forState:UIControlStateNormal];
        [self.signUpButtonModal addTarget:self
                             action:@selector(signUpModalButtonPressed:)
                   forControlEvents:UIControlEventTouchUpInside];
        self.signUpButtonModal.enabled = NO;
        self.signUpButtonModal.titleLabel.alpha = 0.5f;
        [self addSubview:self.signUpButtonModal];
        
        //first responder
        [self.nameTextField becomeFirstResponder];

        //add spinner
        self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(384, 324, 20, 20)];
        [self addSubview:self.spinner];
    }
    return self;
}

#pragma mark signup button behavior

-(IBAction)signUpModalButtonPressed:(id)sender{
    SignupValidation *signUpValidation = [[SignupValidation alloc] init];
    self.validationResultsArray = [signUpValidation validateSignUp:self.nameTextField.text validateEmail:self.emailTextField.text validatePassword:self.passwordTextField.text validateConfirmPassword:self.confirmPasswordTextField.text];
    if ([self.validationResultsArray count] == 0) {
        [self registerUserOnDB];
        
    } else {
        self.validationErrorMessage.text = [self clearTextFieldsBasedOnValidation];
        self.signUpButtonModal.enabled = NO;
        self.signUpButtonModal.titleLabel.alpha = 0.5f;
    }
}

-(void)registerUserOnDB{
    [self.spinner startAnimating];
    self.signUpButtonModal.enabled = NO;
    self.signUpButtonModal.titleLabel.alpha = 0.5f;
    
    NSString *stringToMD5 = [self.passwordTextField.text MD5String];
    
    NSMutableDictionary *registerParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           self.nameTextField.text, @"name",
                                           self.emailTextField.text, @"username",
                                           stringToMD5, @"password",
                                           nil];
    
    [[API sharedInstance] registerUser:registerParams onCompletion:^(NSDictionary *json) {
        //result returned
        if ([json objectForKey:@"error"]) {
            self.validationErrorMessage.text = @"Please connect to the Internet in order to log in";
            [self.emailTextField becomeFirstResponder];
            [self.spinner stopAnimating];
            self.signUpButtonModal.enabled = YES;
            self.signUpButtonModal.titleLabel.alpha = 1;
        }
        
        if ([json objectForKey:@"code"] && [[json objectForKey:@"code"] integerValue] == 0){
            self.validationErrorMessage.text = @"Email address is already taken, please try again";
            self.emailTextField.text = @"";
            [self.emailTextField becomeFirstResponder];
            [self.spinner stopAnimating];
            self.signUpButtonModal.enabled = YES;
            self.signUpButtonModal.titleLabel.alpha = 1;
        }
        
        if ([[json objectForKey:@"code"] integerValue] > 0) {
            [self.spinner stopAnimating];
            self.signUpButtonModal.enabled = YES;
            self.signUpButtonModal.titleLabel.alpha = 1;
            [[API sharedInstance] setUser:json];
            [UICKeyChainStore setString:self.emailTextField.text forKey:@"username" service:@"flee"];
            [UICKeyChainStore setString:stringToMD5 forKey:@"password" service:@"flee"];
            [UIView animateWithDuration:0.5f animations:^{
                [self setFrame:CGRectMake(0, 1004, 768, 1004)];
            } completion:^(BOOL finished) {
                [self removeFromSuperview];
                [self.delegate removeSplashScreen];
            }];
        }
    }
     ];
    
}

#pragma mark textfield validation

-(IBAction)textFieldChangedAction:(UITextField *)sender{
    if ([self.nameTextField.text isEqualToString:@""] || [self.emailTextField.text isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""] || [self.confirmPasswordTextField.text isEqualToString:@""]) {
        self.signUpButtonModal.enabled = NO;
        self.signUpButtonModal.titleLabel.alpha = 0.5f;
    } else {
        self.signUpButtonModal.enabled = YES;
        self.signUpButtonModal.titleLabel.alpha = 1.0f;
    }
}

-(NSString *)clearTextFieldsBasedOnValidation{
    
    int arrayCount = 0;

    if ([[self.validationResultsArray objectAtIndex:arrayCount] isEqualToString:@"name"]) {
        self.nameTextField.text = @"";
        arrayCount++;
        [self.nameTextField becomeFirstResponder];
    }
    
    if ([[self.validationResultsArray objectAtIndex:arrayCount] isEqualToString:@"email"]) {
        arrayCount++;
        if (arrayCount == 1) {
            [self.emailTextField becomeFirstResponder];
        }
    }
    
    if ([[self.validationResultsArray objectAtIndex:arrayCount] isEqualToString:@"password"]) {
        self.passwordTextField.text = @"";
        self.confirmPasswordTextField.text = @"";
        arrayCount++;
        if (arrayCount == 1) {
            [self.passwordTextField becomeFirstResponder];
        }
        if ([self.validationResultsArray containsObject:@"confirm password"]) {
            [self.validationResultsArray removeObject:@"confirm password"];
        }
        return [NSString stringWithFormat:@"Please verify that the %@ fields are correct", [self.validationResultsArray componentsJoinedByString:@", "]];
    }
    
    if ([[self.validationResultsArray objectAtIndex:arrayCount] isEqualToString:@"confirm password"]) {
        
        self.passwordTextField.text = @"";
        self.confirmPasswordTextField.text = @"";
        [self.validationResultsArray replaceObjectAtIndex:arrayCount withObject:@"password"];
        arrayCount++;
        if (arrayCount == 1) {
            [self.passwordTextField becomeFirstResponder];
        }
    }

    return [NSString stringWithFormat:@"Please verify that the %@ fields are correct", [self.validationResultsArray componentsJoinedByString:@", "]];
}

@end