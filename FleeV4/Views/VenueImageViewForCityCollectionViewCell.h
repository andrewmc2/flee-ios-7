//
//  VenueImageViewForCityCollectionViewCell.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueImageViewForCityCollectionViewCell : UIImageView

@property (strong, nonatomic) UILabel *venueLabel;
@property (strong, nonatomic) NSString *venueName;
@property (strong, nonatomic) UITapGestureRecognizer *tapGestureRecognizer;

@end