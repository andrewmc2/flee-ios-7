//
//  LoginActualModalView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "LoginActualModalView.h"
#import "SignupValidation.h"
#import "NSString+MD5.h"
#import "API.h"
#import <UICKeyChainStore.h>
#import "FirstLaunchSingleton.h"

@interface LoginActualModalView ()

@property (strong, nonatomic) UITextField *emailTextField;
@property (strong, nonatomic) UITextField *passwordTextField;
@property (strong, nonatomic) UILabel *validationErrorMessage;
@property (strong, nonatomic) NSMutableArray *validationResultsArray;
@property (strong,nonatomic) UIButton *logInButtonModal;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;

@end

@implementation LoginActualModalView

- (id)init
{
    self = [super init];
    if (self) {
        //add label for textfields
        NSArray *labelNameArray = @[@"Email Address",@"Password"];
        int labelYValue = 210;
        for (int i = 0; i < 2; i++) {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(134, labelYValue, 300, 45)];
            label.font = [UIFont fontWithName:@"FuturaStd-Medium" size:18.0f];
            label.textColor = [UIColor whiteColor];
            label.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
            label.text = [labelNameArray objectAtIndex:i];
            [self addSubview:label];
            labelYValue += 49;
        }
        
        //add textfields
        self.emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 214, 318.5, 35)];
        self.emailTextField.backgroundColor = [UIColor whiteColor];
        self.emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.emailTextField.autocorrectionType = FALSE;
        self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.emailTextField.delegate = self;
        [self.emailTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        self.emailTextField.text = @"testing@setmeflee.com";
        [self addSubview:self.emailTextField];
        
        self.passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(315, 263, 318.5, 35)];
        self.passwordTextField.backgroundColor = [UIColor whiteColor];
        self.passwordTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.passwordTextField.autocorrectionType = FALSE;
        self.passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        [self.passwordTextField addTarget:self action:@selector(textFieldChangedAction:) forControlEvents:UIControlEventEditingChanged];
        self.passwordTextField.text = @"123456789";
        self.passwordTextField.secureTextEntry = YES;
        [self addSubview:self.passwordTextField];
        
        //validation error message
        self.validationErrorMessage = [[UILabel alloc]initWithFrame:CGRectMake(134, 165, 500, 35)];
        self.validationErrorMessage.text = @"";
        [self.validationErrorMessage setTextColor:[UIColor whiteColor]];
        self.validationErrorMessage.backgroundColor = [UIColor clearColor];
        [self.validationErrorMessage setFont:[UIFont fontWithName:@"FuturaStd-Medium" size:18.0f]];
        [self addSubview:self.validationErrorMessage];
        
        //sign up button
        self.logInButtonModal = [UIButton buttonWithType:UIButtonTypeCustom];
        self.logInButtonModal.frame = CGRectMake(509, 420, 125, 33.5);
        [self.logInButtonModal setBackgroundImage:[UIImage imageNamed:@"signupLoginButtonBG.png"]  forState:UIControlStateNormal];
        [self.logInButtonModal setBackgroundImage:[UIImage imageNamed:@"signUpLoginButtonHighlitedBG.png"] forState:UIControlStateHighlighted];
        [self.logInButtonModal.titleLabel setTextColor:[UIColor whiteColor]];
        self.logInButtonModal.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:19];
        [self.logInButtonModal setTitle:@"LOG IN" forState:UIControlStateNormal];
        [self.logInButtonModal addTarget:self
                                   action:@selector(logInModalButtonPressed:)
                         forControlEvents:UIControlEventTouchUpInside];
        self.logInButtonModal.enabled = NO;
        self.logInButtonModal.titleLabel.alpha = 0.5f;
        [self addSubview:self.logInButtonModal];
        
        //first responder
        [self.emailTextField becomeFirstResponder];
        
        //add spinner
        self.spinner = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(384, 324, 20, 20)];
        [self addSubview:self.spinner];
        
        self.logInButtonModal.enabled = YES;
        self.logInButtonModal.titleLabel.alpha = 1.0f;
    }
    return self;
}

#pragma mark logIn button behavior

-(IBAction)logInModalButtonPressed:(id)sender{

    [self loginUserOnDB];
        
}

-(void)loginUserOnDB{
    [self.spinner startAnimating];
    self.logInButtonModal.enabled = NO;
    self.logInButtonModal.titleLabel.alpha = 0.5f;
    
    NSString *stringToMD5 = [self.passwordTextField.text MD5String];
    
    NSMutableDictionary *registerParams = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           self.emailTextField.text, @"username",
                                           stringToMD5, @"password",
                                           nil];
    
    [[API sharedInstance] loginWithParams:registerParams onCompletion:^(NSDictionary *json) {
        //result returned
        if ([json objectForKey:@"error"]) {
            self.validationErrorMessage.text = @"Please connect to the Internet in order to log in";
            [self.emailTextField becomeFirstResponder];
            [self.spinner stopAnimating];
            self.logInButtonModal.enabled = YES;
            self.logInButtonModal.titleLabel.alpha = 1;
        }
        
        if ([json objectForKey:@"code"] && [[json objectForKey:@"code"] integerValue] == 0){
            self.validationErrorMessage.text = @"Incorrect email and/or password, please try again";
            [self.emailTextField becomeFirstResponder];
            [self.spinner stopAnimating];
            self.logInButtonModal.enabled = YES;
            self.logInButtonModal.titleLabel.alpha = 1;
        }
        
        if ([[json objectForKey:@"code"] integerValue] > 0) {
            self.logInButtonModal.enabled = YES;
            self.logInButtonModal.titleLabel.alpha = 1;
            [self.spinner stopAnimating];
            [[API sharedInstance] setUser:json];
            [UICKeyChainStore setString:self.emailTextField.text forKey:@"username" service:@"flee"];
            [UICKeyChainStore setString:stringToMD5 forKey:@"password" service:@"flee"];
            [UIView animateWithDuration:0.5f animations:^{
                [self setFrame:CGRectMake(0, 1004, 768, 1004)];
            } completion:^(BOOL finished) {
                [self removeFromSuperview];
                [self.delegate removeSplashScreen];
            }];
        }
    }
     ];
    
}

#pragma mark textfield validation

-(IBAction)textFieldChangedAction:(UITextField *)sender{
    if ([self.emailTextField.text isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""]) {
        self.logInButtonModal.enabled = NO;
        self.logInButtonModal.titleLabel.alpha = 0.5f;
    } else {
        self.logInButtonModal.enabled = YES;
        self.logInButtonModal.titleLabel.alpha = 1.0f;
    }
}
@end
