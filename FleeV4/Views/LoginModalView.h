//
//  LoginModalView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-22.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginSignUpDelegate.h"

@interface LoginModalView : UIView

@property (nonatomic, weak) UIButton *closeModalView;
@property (nonatomic,weak) id <LoginSignUpDelegate> delegate;

@end
