//
//  MainPageSearchAndCollectionViewScrollView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "MainPageSearchAndCollectionViewScrollView.h"
#import "MainPageCollectionViewFlowLayout.h"
#import "MainPageCollectionViewCustomCell.h"
#import "SearchIconView.h"

@implementation MainPageSearchAndCollectionViewScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 486, 768, 798);
        [self setContentSize:CGSizeMake(1536, 798)];
        self.pagingEnabled = YES;
        self.scrollEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        
        [self createSearchAndFavoriteBar];
        [self createMainPageCollectionView];
        [self createFavoritesCollectionView];
    }
    return self;
}

-(void)createSearchAndFavoriteBar{
    //add search view.
    SearchIconView *searchIconView = [[SearchIconView alloc] init];
    [self addSubview:searchIconView];
    
    //favorites label
    UILabel *favoritesLabel = [[UILabel alloc] initWithFrame:CGRectMake(769, 40, 768, 60)];
    favoritesLabel.text = @"FAVORITES";
    favoritesLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
    favoritesLabel.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
    favoritesLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:favoritesLabel];
}

-(void)createMainPageCollectionView{
    //add city search collectionview
    MainPageCollectionViewFlowLayout *mainPageFlowLayout = [[MainPageCollectionViewFlowLayout alloc] init];
    self.mainPageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 131, 768, 699) collectionViewLayout:mainPageFlowLayout];
    [self.mainPageCollectionView registerClass:[MainPageCollectionViewCustomCell class] forCellWithReuseIdentifier:@"itemIdentifier"];
    self.mainPageCollectionView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.mainPageCollectionView];
}

-(void)createFavoritesCollectionView{
    //add favorites collectionview
    MainPageCollectionViewFlowLayout *favoritesFlowLayout = [[MainPageCollectionViewFlowLayout alloc] init];
    self.favoritesCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(768, 131, 768, 699) collectionViewLayout:favoritesFlowLayout];
    [self.favoritesCollectionView registerClass:[MainPageCollectionViewCustomCell class] forCellWithReuseIdentifier:@"itemIdentifier"];
    self.favoritesCollectionView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3f];
    [self addSubview:self.favoritesCollectionView];
}

@end
