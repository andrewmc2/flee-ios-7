//
//  CityViewControllerCollectionViewCustomCell.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "CityViewControllerCollectionViewCustomCell.h"


@implementation CityViewControllerCollectionViewCustomCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.cellOriginalContentSize = 1023;
        //set size
        self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 768, 1284)];
        [self.mainScrollView setContentSize:CGSizeMake(768, self.cellOriginalContentSize)];
        
        [self addSubview:self.mainScrollView];
         
        self.backgroundColor = [UIColor whiteColor];
        
        //add image
        self.cityImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 260, 768, 380)];
        [self.mainScrollView addSubview:self.cityImageView];
        
        //back button
        self.backButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.backButton.frame = CGRectMake(10, 275, 80, 40);
        [self.backButton setTitle:@"< Back" forState:UIControlStateNormal];
        [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.mainScrollView addSubview:self.backButton];
        
        //add logo
        //logo return button
        self.logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.logoButton.frame = CGRectMake(72, 381, 131, 50);
        [self.logoButton setBackgroundImage:[UIImage imageNamed:@"fleeLogoWhite.png"] forState:UIControlStateNormal];
        [self.mainScrollView addSubview:self.logoButton];
        
        //cityname
        self.cityName = [[UILabel alloc] initWithFrame:CGRectMake(85, 575, 550, 40)];
        self.cityName.font = [UIFont fontWithName:@"FuturaStd-Medium" size:25.0f];
        self.cityName.textColor = [UIColor whiteColor];
        self.cityName.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.cityName];
        
        //add heart
        self.favoritedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.favoritedButton.frame = CGRectMake(670, 575, 30, 30);
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_red"] forState:UIControlStateSelected];
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_white"] forState:UIControlStateNormal];
        [self.mainScrollView addSubview:self.favoritedButton];
        
        //info scrollview
        self.infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 640, 768, 383)];
        self.infoScrollView.delegate = self;
        self.infoScrollView.backgroundColor = [UIColor colorWithRed:195.0 green:195.0 blue:195.0 alpha:0.7f];
        [self.infoScrollView setContentSize:CGSizeMake(2304, 280)];
        self.infoScrollView.pagingEnabled = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.showsHorizontalScrollIndicator = NO;
        self.infoScrollView.backgroundColor = [UIColor whiteColor];
        [self.mainScrollView addSubview:self.infoScrollView];
        
        //textview inside info scrollview (figure out line spacing either through custom font or custom class)
        self.cityDescriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(85, 40, 598, 265)];
        self.cityDescriptionTextView.font = [UIFont fontWithName:@"EgyptienneFLTStd-Roman" size:16.0f];
        self.cityDescriptionTextView.textColor = [UIColor colorWithRed:(109.0/255.0f) green:(107.0/255.0f) blue:(120.0/255.0f) alpha:1.0f];
        self.cityDescriptionTextView.backgroundColor = [UIColor clearColor];
        self.cityDescriptionTextView.editable = NO;
        [self.infoScrollView addSubview:self.cityDescriptionTextView];
        
        //temp content labels
        self.cityDescriptionTextView2temp = [[UILabel alloc] initWithFrame:CGRectMake(769, 0, 768, 383)];
        self.cityDescriptionTextView2temp.text = @"CONTENT TBD";
        self.cityDescriptionTextView2temp.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.cityDescriptionTextView2temp.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.cityDescriptionTextView2temp.textAlignment = NSTextAlignmentCenter;
        [self.infoScrollView addSubview:self.cityDescriptionTextView2temp];
        
        self.cityDescriptionTextView3temp = [[UILabel alloc] initWithFrame:CGRectMake(1537, 0, 768, 383)];
        self.cityDescriptionTextView3temp.text = @"CONTENT TBD";
        self.cityDescriptionTextView3temp.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.cityDescriptionTextView3temp.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.cityDescriptionTextView3temp.textAlignment = NSTextAlignmentCenter;
        [self.infoScrollView addSubview:self.cityDescriptionTextView3temp];
        
        //page control for options scrollview
        self.infoPageControlForOptions = [[UIPageControl alloc] initWithFrame:CGRectMake(359, 983, 60, 20)];
        self.infoPageControlForOptions.numberOfPages = 3;
        self.infoPageControlForOptions.pageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
        self.infoPageControlForOptions.currentPageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
        [self.mainScrollView addSubview:self.infoPageControlForOptions];
        
        //social media share button
        self.shareToSocialMediaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.shareToSocialMediaButton setBackgroundImage:[UIImage imageNamed:@"shareIcon"] forState:UIControlStateNormal];
        self.shareToSocialMediaButton.frame = CGRectMake(85, 300, 17.5, 25);
        [self.infoScrollView addSubview:self.shareToSocialMediaButton];
        
        //book flight button
        self.bookFlightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bookFlightButton.frame = CGRectMake(558, 300, 120, 35);
        [self.bookFlightButton setBackgroundImage:[UIImage imageNamed:@"signupLoginButtonBG"] forState:UIControlStateNormal];
        [self.bookFlightButton setBackgroundImage:[UIImage imageNamed:@"signUpLoginButtonHighlitedBG.png"] forState:UIControlStateHighlighted];
        self.bookFlightButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:15.0f];
        [self.bookFlightButton setTitle: @"BOOK FLIGHT" forState:UIControlStateNormal];
        [self.bookFlightButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.bookFlightButton.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.infoScrollView addSubview:self.bookFlightButton];
        
        //venue views
        //calc amount
    }
    return self;
}

-(void)setImage:(UIImage *)image
{
    self.cityImageView.image = image;
}

-(void)setContentSize:(int)yContentSize{
    int contentSizeNow = self.cellOriginalContentSize+145*yContentSize;
    [self.mainScrollView setContentSize:CGSizeMake(768, contentSizeNow)];
}

-(void)addVenueToCollectionViewCell: (int)yPosition{
    self.venueImageView = [[VenueImageViewForCityCollectionViewCell alloc] initWithFrame:CGRectMake(0, yPosition, 768, 145)];
    [self.mainScrollView addSubview:self.venueImageView];
}

#pragma mark scroll view delegate methods
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//this changes the page control value when scrolling in the optionScrollView
    if (scrollView == self.infoScrollView) {
        self.infoPageControlForOptions.currentPage = self.infoScrollView.contentOffset.x/scrollView.frame.size.width;
    }
}

@end
