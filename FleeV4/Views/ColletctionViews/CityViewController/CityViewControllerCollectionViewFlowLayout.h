//
//  CityViewControllerCollectionViewFlowLayout.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CityViewControllerCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (nonatomic) CGSize itemSize;

@end
