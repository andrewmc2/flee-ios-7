//
//  CityViewControllerCollectionViewFlowLayout.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "CityViewControllerCollectionViewFlowLayout.h"

@implementation CityViewControllerCollectionViewFlowLayout

- (id)init
{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(768, 1284);
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        //        self.footerReferenceSize = CGSizeMake(768, 145);
        return self;
    }
    return self;
}

@end
