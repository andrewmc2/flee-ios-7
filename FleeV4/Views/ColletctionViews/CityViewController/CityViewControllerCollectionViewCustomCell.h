//
//  CityViewControllerCollectionViewCustomCell.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-25.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "VenueImageViewForCityCollectionViewCell.h"

@interface CityViewControllerCollectionViewCustomCell : UICollectionViewCell <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *mainScrollView;
@property (strong, nonatomic) UIImageView *cityImageView;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UIButton *logoButton;
@property (strong, nonatomic) UILabel *cityName;
@property (nonatomic, strong) UIButton *favoritedButton;
@property (strong, nonatomic) NSString *cityURL;
@property (strong, nonatomic) UIScrollView *infoScrollView;
@property (nonatomic, strong) UIPageControl *infoPageControlForOptions;
@property (strong, nonatomic) UILabel *cityDescriptionTextView2temp;
@property (strong, nonatomic) UILabel *cityDescriptionTextView3temp;
@property (strong, nonatomic) UITextView *cityDescriptionTextView;
@property (strong, nonatomic) UIButton *shareToSocialMediaButton;
@property (strong, nonatomic) UIButton *bookFlightButton;
@property (strong, nonatomic) VenueImageViewForCityCollectionViewCell *venueImageView;
@property (nonatomic) int cellOriginalContentSize;

-(void)setImage:(UIImage *)image;
-(void)setContentSize:(int)yContentSize;
-(void)addVenueToCollectionViewCell: (int)yPosition;

@end
