//
//  MainPageCollectionViewCustomCell.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "MainPageCollectionViewCustomCell.h"

@implementation MainPageCollectionViewCustomCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 145)];
        [self.contentView addSubview:self.imageView];
        self.backgroundColor = [UIColor whiteColor];
        
        //add city name
        self.cityName = [[UILabel alloc] initWithFrame:CGRectMake(85, 70, 668, 70)];
        self.cityName.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.cityName.textColor = [UIColor whiteColor];
        self.cityName.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.cityName];
        
        //add heart
        self.favoritedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.favoritedButton.frame = CGRectMake(670, 100, 30, 30);
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_red"] forState:UIControlStateSelected];
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_white"] forState:UIControlStateNormal];
        //        [self.favoritedButton addTarget:self action:@selector(favoritedButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.favoritedButton];

        
        return self;
    }
    return self;
}

-(void)setImage:(UIImage *)image
{
    self.imageView.image = image;
}

@end
