//
//  MainPageCollectionViewCustomCell.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainPageCollectionViewCustomCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *cityName;
@property (nonatomic, strong) UIButton *favoritedButton;
@property (nonatomic, strong) NSString *cityURL;

//for venue
@property (nonatomic, strong) NSString *venueCode;

-(void)setImage:(UIImage *)image;

@end
