//
//  VenueViewControllerCollectionViewCustomCell.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-26.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VenueViewControllerCollectionViewCustomCell : UICollectionViewCell <UIScrollViewDelegate>

@property (strong, nonatomic) UIScrollView *mainScrollView;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIImageView *venueImageView;
@property (strong, nonatomic) UIButton *logoButton;
@property (strong, nonatomic) UIButton *backButton;
@property (strong, nonatomic) UILabel *venueName;
@property (nonatomic, strong) UIButton *favoritedButton;
@property (strong, nonatomic) NSString *cityURL;
@property (strong, nonatomic) NSString *venueURL;
@property (strong, nonatomic) UIScrollView *infoScrollView;
@property (strong, nonatomic) UITextView *venueDescriptionTextView;
@property (strong, nonatomic) UILabel *venueDescriptionTextView2temp;
@property (strong, nonatomic) UILabel *venueDescriptionTextView3temp;
@property (nonatomic, strong) UIPageControl *infoPageControlForOptions;
@property (strong, nonatomic) UIButton *shareToSocialMediaButton;
@property (strong, nonatomic) UIButton *bookHotelButton;

@end