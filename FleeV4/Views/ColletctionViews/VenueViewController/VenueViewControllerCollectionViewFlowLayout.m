//
//  VenueViewControllerCollectionViewFlowLayout.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-26.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "VenueViewControllerCollectionViewFlowLayout.h"

@implementation VenueViewControllerCollectionViewFlowLayout

- (id)init
{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(768, 640);
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        return self;
    }
    return self;
}


@end
