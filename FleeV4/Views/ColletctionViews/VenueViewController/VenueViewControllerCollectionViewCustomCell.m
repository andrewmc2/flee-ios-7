//
//  VenueViewControllerCollectionViewCustomCell.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-26.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "VenueViewControllerCollectionViewCustomCell.h"

@implementation VenueViewControllerCollectionViewCustomCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        //add image
        self.venueImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 380)];
        [self addSubview:self.venueImageView];
        
        //back button
        self.backButton = [UIButton buttonWithType:UIButtonTypeSystem];
        self.backButton.frame = CGRectMake(10, 15, 80, 40);
        [self.backButton setTitle:@"< Back" forState:UIControlStateNormal];
        [self.backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:self.backButton];
        
        //logo return button
        self.logoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.logoButton.frame = CGRectMake(72, 121, 131, 50);
        [self.logoButton setBackgroundImage:[UIImage imageNamed:@"fleeLogoWhite.png"] forState:UIControlStateNormal];
        [self addSubview:self.logoButton];
        
        //add venue name
        self.venueName = [[UILabel alloc] initWithFrame:CGRectMake(85, 315, 350, 40)];
        self.venueName.font = [UIFont fontWithName:@"FuturaStd-Medium" size:20.0f];
        self.venueName.textColor = [UIColor whiteColor];
        self.venueName.backgroundColor = [UIColor clearColor];
        [self addSubview:self.venueName];
        
        //add heart
        self.favoritedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.favoritedButton.frame = CGRectMake(670, 315, 30, 30);
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_red"] forState:UIControlStateSelected];
        [self.favoritedButton setImage:[UIImage imageNamed:@"heart_white"] forState:UIControlStateNormal];
        [self addSubview:self.favoritedButton];
        
        //info scrollview
        self.infoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 381, 768, 260)];
        self.infoScrollView.delegate = self;
        [self.infoScrollView setBackgroundColor:[UIColor redColor]];
        self.infoScrollView.backgroundColor = [UIColor colorWithRed:195.0 green:195.0 blue:195.0 alpha:0.7f];
        [self.infoScrollView setContentSize:CGSizeMake(2304, 260)];
        self.infoScrollView.pagingEnabled = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.showsHorizontalScrollIndicator = NO;
        self.infoScrollView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.infoScrollView];
        
        //textview inside info scrollview (figure out line spacing either through custom font or custom class)
        self.venueDescriptionTextView = [[UITextView alloc] initWithFrame:CGRectMake(85, 40, 598, 150)];
        self.venueDescriptionTextView.font = [UIFont fontWithName:@"EgyptienneFLTStd-Roman" size:16.0f];
        self.venueDescriptionTextView.textColor = [UIColor colorWithRed:(109.0/255.0f) green:(107.0/255.0f) blue:(120.0/255.0f) alpha:1.0f];
        self.venueDescriptionTextView.backgroundColor = [UIColor clearColor];
        self.venueDescriptionTextView.editable = NO;
        [self.infoScrollView addSubview:self.venueDescriptionTextView];
        
        //temp textviews
        self.venueDescriptionTextView2temp = [[UILabel alloc] initWithFrame:CGRectMake(769, 0, 768, 260)];
        self.venueDescriptionTextView2temp.text = @"CONTENT TBD";
        self.venueDescriptionTextView2temp.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.venueDescriptionTextView2temp.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.venueDescriptionTextView2temp.textAlignment = NSTextAlignmentCenter;
        [self.infoScrollView addSubview:self.venueDescriptionTextView2temp];
        
        self.venueDescriptionTextView3temp = [[UILabel alloc] initWithFrame:CGRectMake(1537, 0, 768, 260)];
        self.venueDescriptionTextView3temp.text = @"CONTENT TBD";
        self.venueDescriptionTextView3temp.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.venueDescriptionTextView3temp.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.venueDescriptionTextView3temp.textAlignment = NSTextAlignmentCenter;
        [self.infoScrollView addSubview:self.venueDescriptionTextView3temp];
        
        //page control for options scrollview
        self.infoPageControlForOptions = [[UIPageControl alloc] initWithFrame:CGRectMake(359, 601, 60, 20)];
        self.infoPageControlForOptions.numberOfPages = 3;
        self.infoPageControlForOptions.pageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
        self.infoPageControlForOptions.currentPageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
        [self addSubview:self.infoPageControlForOptions];
        
        //social media share button
        self.shareToSocialMediaButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.shareToSocialMediaButton setBackgroundImage:[UIImage imageNamed:@"shareIcon"] forState:UIControlStateNormal];
        self.shareToSocialMediaButton.frame = CGRectMake(85, 200, 17.5, 25);
        [self.infoScrollView addSubview:self.shareToSocialMediaButton];
        
        //book flight button
        self.bookHotelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.bookHotelButton.frame = CGRectMake(558, 200, 120, 35);
        [self.bookHotelButton setBackgroundImage:[UIImage imageNamed:@"signupLoginButtonBG"] forState:UIControlStateNormal];
        [self.bookHotelButton setBackgroundImage:[UIImage imageNamed:@"signUpLoginButtonHighlitedBG.png"] forState:UIControlStateHighlighted];
        self.bookHotelButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:15.0f];
        [self.bookHotelButton setTitle: @"BOOK HOTEL" forState:UIControlStateNormal];
        [self.bookHotelButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.bookHotelButton.titleLabel setBackgroundColor:[UIColor clearColor]];
//        self.bookHotelButton.hidden = YES;
        [self.infoScrollView addSubview:self.bookHotelButton];
    }
    return self;
}

#pragma mark scroll view delegate methods
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //this changes the page control value when scrolling in the optionScrollView
    if (scrollView == self.infoScrollView) {
        self.infoPageControlForOptions.currentPage = self.infoScrollView.contentOffset.x/scrollView.frame.size.width;
    }
}
@end
