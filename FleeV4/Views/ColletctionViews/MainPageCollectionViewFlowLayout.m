//
//  MainPageCollectionViewFlowLayout.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "MainPageCollectionViewFlowLayout.h"

@implementation MainPageCollectionViewFlowLayout

- (id)init
{
    self = [super init];
    if (self) {
        self.itemSize = CGSizeMake(768, 145);
        //    self.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
        self.minimumInteritemSpacing = 0;
        self.minimumLineSpacing = 0;
//        self.footerReferenceSize = CGSizeMake(768, 145);
        return self;
    }
    return self;
}

@end
