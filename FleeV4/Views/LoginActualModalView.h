//
//  LoginActualModalView.h
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-23.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "LoginModalView.h"

@interface LoginActualModalView : LoginModalView <UITextFieldDelegate>

@end
