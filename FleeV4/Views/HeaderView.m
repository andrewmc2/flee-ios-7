//
//  HeaderView.m
//  FleeV4
//
//  Created by Andrew McCallum14 on 2013-09-21.
//  Copyright (c) 2013 Andrew McCallum. All rights reserved.
//

#import "HeaderView.h"

@interface HeaderView ()

@property (strong, nonatomic) UIScrollView *optionScrollView;
@property (strong, nonatomic) UILabel *headerTempLabel2;
@property (strong, nonatomic) UILabel *headerTempLabel3;
@property (strong, nonatomic) UILabel *headerTempLabel4;

@end

@implementation HeaderView

- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0, 0, 768, 486);
        self.backgroundColor = [UIColor clearColor];
        
        //imageview
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 486)];
        [imageView setImage:[UIImage imageNamed:@"mainPageHeaderBG.png"]];
        [self addSubview:imageView];
        
        //view for options scrollview
        UIView *viewForOptionsScrollView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 260)];
        [self addSubview:viewForOptionsScrollView];
        
        //add logo
        UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(72, 381, 131, 50)];
        [logoImageView setImage:[UIImage imageNamed:@"fleeLogoWhite.png"]];
        [self addSubview:logoImageView];
        
        //options scrollview
        self.optionScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 768, 260)];
        self.optionScrollView.delegate = self;
        self.optionScrollView.backgroundColor = [UIColor colorWithRed:195.0 green:195.0 blue:195.0 alpha:0.7f];
        [self.optionScrollView setContentSize:CGSizeMake(2304, 260)];
        self.optionScrollView.pagingEnabled = YES;
        self.optionScrollView.scrollEnabled = YES;
        self.optionScrollView.showsHorizontalScrollIndicator = NO;
        [viewForOptionsScrollView addSubview:self.optionScrollView];
        
        //logout button for optionsscrollview
        self.logOutButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.logOutButton.frame = CGRectMake(625, 40, 90, 30);
        [self.logOutButton setBackgroundImage:[UIImage imageNamed:@"signupLoginButtonBG"] forState:UIControlStateNormal];
        [self.logOutButton setBackgroundImage:[UIImage imageNamed:@"signUpLoginButtonHighlitedBG.png"] forState:UIControlStateHighlighted];
        self.logOutButton.titleLabel.font = [UIFont fontWithName:@"FuturaStd-Medium" size:15.0f];
        [self.logOutButton setTitle: @"LOG OUT" forState:UIControlStateNormal];
        [self.logOutButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.logOutButton.titleLabel setBackgroundColor:[UIColor clearColor]];
        [self.optionScrollView addSubview:self.logOutButton];
        
        //page control for options scrollview
        self.pageControlForOptions = [[UIPageControl alloc] initWithFrame:CGRectMake(354, 235, 60, 20)];
        self.pageControlForOptions.numberOfPages = 3;
        self.pageControlForOptions.pageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.4f];
        self.pageControlForOptions.currentPageIndicatorTintColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1.0f];
        [viewForOptionsScrollView addSubview:self.pageControlForOptions];
        
        //temp content labels
        self.headerTempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 100, 768, 60)];
        self.headerTempLabel2.text = @"CONTENT TBD";
        self.headerTempLabel2.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.headerTempLabel2.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.headerTempLabel2.textAlignment = NSTextAlignmentCenter;
        [self.optionScrollView addSubview:self.headerTempLabel2];
        
        self.headerTempLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(769, 100, 768, 60)];
        self.headerTempLabel3.text = @"CONTENT TBD";
        self.headerTempLabel3.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.headerTempLabel3.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.headerTempLabel3.textAlignment = NSTextAlignmentCenter;
        [self.optionScrollView addSubview:self.headerTempLabel3];

        self.headerTempLabel4 = [[UILabel alloc] initWithFrame:CGRectMake(1537, 100, 768, 60)];
        self.headerTempLabel4.text = @"CONTENT TBD";
        self.headerTempLabel4.font = [UIFont fontWithName:@"FuturaStd-Medium" size:60.0f];
        self.headerTempLabel4.textColor = [UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0];
        self.headerTempLabel4.textAlignment = NSTextAlignmentCenter;
        [self.optionScrollView addSubview:self.headerTempLabel4];
        
        //needs editing
        //about content
//        UITextView *aboutTitleTextView = [[UITextView alloc] initWithFrame:CGRectMake(1570, 50, 150, 25)];
//        [aboutTitleTextView setTextColor:[UIColor whiteColor]];
//        [aboutTitleTextView setFont:[UIFont fontWithName:@"FuturaStd-Medium" size:20.0f]];
//        [aboutTitleTextView setBackgroundColor:[UIColor clearColor]];
//        aboutTitleTextView.text = @"ABOUT";
//        [self.optionScrollView addSubview:aboutTitleTextView];
//        
//        UITextView *aboutTextView = [[UITextView alloc] initWithFrame:CGRectMake(1570, 80, 700, 150)];
//        [aboutTextView setTextColor:[UIColor colorWithRed:(211/255.0f) green:(113/255.0f) blue:(86/255.0f) alpha:1.0]];
//        [aboutTextView setFont:[UIFont fontWithName:@"EgyptienneFLTStd-Roman" size:16.0f]];
//        [aboutTextView setBackgroundColor:[UIColor clearColor]];
//        aboutTextView.text = @"We at Flee believe that life is at it's best when seen from the perspective of the Peripathete. A portmanteau of: Peripatetic \[travelling from place to place] and Aesthete \[a refined sensitivity towards art or nature]. We hope that you feel the same way. Flee is lovingly made in Toronto. We'd be thrilled to hear from you, so feel free to get in touch with questions, photographs, venue suggestions, kudos, unqualified criticisms, or laksa recipes, at info@setmeflee.com";
//        [self.optionScrollView addSubview:aboutTextView];
        
        //ribbon pull down
        self.ribbonPullDown = [UIButton buttonWithType:UIButtonTypeCustom];
        self.ribbonPullDown.frame = CGRectMake(666.5, 260, 35.0, 88.0);
        [self.ribbonPullDown setImage:[UIImage imageNamed:@"ribbonPullDown.png"] forState:UIControlStateNormal];
        [self addSubview:self.ribbonPullDown];
    }
    return self;
}

#pragma mark UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //this changes the page control value when scrolling in the optionScrollView
    self.pageControlForOptions.currentPage = self.optionScrollView.contentOffset.x/scrollView.frame.size.width;
}

@end